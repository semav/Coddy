import {
    AUTH_INIT,
    CREATE_DOCUMENT_ERROR,
    CREATE_DOCUMENT_REQUEST,
    CREATE_DOCUMENT_SUCCESS,
    DELETE_DOCUMENT_SUCCESS,
    FETCH_DOCUMENT_ERROR,
    FETCH_DOCUMENT_REQUEST,
    FETCH_DOCUMENT_SUCCESS,
    FETCH_DOCUMENTS_ERROR,
    FETCH_DOCUMENTS_REQUEST,
    FETCH_DOCUMENTS_SUCCESS,
    RUN_DOCUMENT_ERROR,
    RUN_DOCUMENT_REQUEST,
    SET_DOCUMENT_CONTENT,
    SET_DOCUMENT_MODE_ERROR,
    SET_DOCUMENT_MODE_REQUEST,
    SET_DOCUMENT_MODE_SUCCESS,
    SET_DOCUMENT_NAME,
    SET_DOCUMENT_SAVED,
    SET_DOCUMENT_SHARE_ERROR,
    SET_DOCUMENT_SHARE_REQUEST,
    SET_DOCUMENT_SHARE_SUCCESS,
    SHOW_EXECUTION_RESULT,
    SING_IN,
    SING_OUT
} from './Actions';

const defaultState = {
    authInit: false,
    user: null,
    documentsList: {
        loading: false,
        documents: []
    },
    documentPage: {
        loading: false,
        document: null,
        documentModeLoading: false,
        outputVisible: false,
        running: false,
        output: null
    },
    createDocumentLoading: false
};

export default (state = defaultState, action) => {
    switch (action.type) {
        case AUTH_INIT:
            return {
                ...state,
                authInit: true
            };
        case SING_IN:
            return {
                ...state,
                user: action.payload,
            };
        case SING_OUT:
            return {
                ...state,
                user: null
            };
        case FETCH_DOCUMENTS_REQUEST:
            return {
                ...state,
                documentsList: {
                    loading: true,
                    documents: []
                }
            };
        case FETCH_DOCUMENTS_SUCCESS:
            return {
                ...state,
                documentsList: {
                    loading: false,
                    documents: action.payload
                }
            };
        case FETCH_DOCUMENTS_ERROR:
            return {
                ...state,
                documentsList: {
                    loading: false,
                    documents: []
                }
            };
        case FETCH_DOCUMENT_REQUEST:
            return {
                ...state,
                documentPage: {
                    loading: true,
                    document: null,
                    content: null,
                    documentModeLoading: false,
                    outputVisible: false,
                    running: false,
                    output: null
                }
            };
        case FETCH_DOCUMENT_SUCCESS:
            return {
                ...state,
                documentPage: {
                    loading: false,
                    document: {saved: true, ...action.payload.document}
                }
            };
        case FETCH_DOCUMENT_ERROR:
            return {
                ...state,
                documentPage: {
                    loading: false,
                    document: null,
                    content: null
                }
            };
        case DELETE_DOCUMENT_SUCCESS:
            return {
                ...state,
                documentsList: {
                    loading: false,
                    documents: state.documentsList.documents.filter(d => d.id !== action.payload)
                }
            };
        case CREATE_DOCUMENT_REQUEST:
            return {
                ...state,
                createDocumentLoading: true
            };
        case CREATE_DOCUMENT_SUCCESS:
        case CREATE_DOCUMENT_ERROR:
            return {
                ...state,
                createDocumentLoading: false
            };
        case SET_DOCUMENT_MODE_REQUEST:
            return {
                ...state,
                documentPage: {
                    ...state.documentPage,
                    documentModeLoading: true,
                }
            };
        case SET_DOCUMENT_MODE_SUCCESS:
            return {
                ...state,
                documentPage: {
                    ...state.documentPage,
                    document: {
                        ...state.documentPage.document,
                        mode: action.payload.mode
                    },
                    documentModeLoading: false,
                }
            };
        case SET_DOCUMENT_MODE_ERROR:
            return {
                ...state,
                documentPage: {
                    ...state.documentPage,
                    documentModeLoading: false,
                }
            };
        case SET_DOCUMENT_SHARE_REQUEST:
            return {
                ...state,
                documentsList: {
                    loading: false,
                    documents: state.documentsList.documents
                        .map(d => d.id === action.payload ? {...d, shareLoading: true} : d)
                }
            };
        case SET_DOCUMENT_SHARE_SUCCESS:
            return {
                ...state,
                documentsList: {
                    loading: false,
                    documents: state.documentsList.documents
                        .map(d => d.id === action.payload.id ? action.payload : d)
                }
            };
        case SET_DOCUMENT_SHARE_ERROR:
            return {
                ...state,
                documentsList: {
                    loading: false,
                    documents: state.documentsList.documents
                        .map(d => d.id === action.payload ? {...d, shareLoading: false} : d)
                }
            };
        case SET_DOCUMENT_NAME:
            return {
                ...state,
                documentPage: {
                    ...state.documentPage,
                    document: {
                        ...state.documentPage.document,
                        name: action.payload
                    },
                    documentModeLoading: false,
                }
            };
        case SET_DOCUMENT_CONTENT:
            return {
                ...state,
                documentPage: {
                    ...state.documentPage,
                    document: {
                        ...state.documentPage.document,
                        content: action.payload
                    },
                }
            };
        case SET_DOCUMENT_SAVED:
            return {
                ...state,
                documentPage: {
                    ...state.documentPage,
                    document: {
                        ...state.documentPage.document,
                        saved: action.payload
                    },
                }
            };
        case RUN_DOCUMENT_REQUEST:
            return {
                ...state,
                documentPage: {
                    ...state.documentPage,
                    running: true,
                    outputVisible: true,
                    output: null
                }
            };
        case RUN_DOCUMENT_ERROR:
            return {
                ...state,
                documentPage: {
                    ...state.documentPage,
                    running: false,
                    outputVisible: false,
                    output: null
                }
            };
        case SHOW_EXECUTION_RESULT:
            const status = action.payload?.status;
            const message = action.payload?.message;

            return {
                ...state,
                documentPage: {
                    ...state.documentPage,
                    running: getRunning(state.documentPage.running, status),
                    outputVisible: state.documentPage.outputVisible || status === 'STARTED',
                    output: getOutput(state.documentPage.output, message, status)
                }
            };
        default:
            break;
    }

    return state;
};

const getRunning = (current, status) => {
    if (status === 'STARTED') {
        return true;
    }

    return current && (status !== 'COMPLETED' && status !== 'ERROR');
};

const getOutput = (current, more, status) => {
    if (status === 'STARTED') {
        return null;
    }

    if (!more) {
        return current;
    }

    if (!current) {
        return more;
    }

    return `${current}\n${more}`;
};