import Api from '../api';
import {errorMessage} from '../message';

export const AUTH_INIT = 'AUTH_INIT';
export const SING_IN = 'SING_IN';
export const SING_OUT = 'SING_OUT';
export const FETCH_DOCUMENTS_REQUEST = 'FETCH_DOCUMENTS_REQUEST';
export const FETCH_DOCUMENTS_SUCCESS = 'FETCH_DOCUMENTS_SUCCESS';
export const FETCH_DOCUMENTS_ERROR = 'FETCH_DOCUMENTS_ERROR';
export const FETCH_DOCUMENT_REQUEST = 'FETCH_DOCUMENT_REQUEST';
export const FETCH_DOCUMENT_SUCCESS = 'FETCH_DOCUMENT_SUCCESS';
export const FETCH_DOCUMENT_ERROR = 'FETCH_DOCUMENT_ERROR';
export const DELETE_DOCUMENT_SUCCESS = 'DELETE_DOCUMENT_SUCCESS';
export const CREATE_DOCUMENT_REQUEST = 'CREATE_DOCUMENT_REQUEST';
export const CREATE_DOCUMENT_SUCCESS = 'CREATE_DOCUMENT_SUCCESS';
export const CREATE_DOCUMENT_ERROR = 'CREATE_DOCUMENTS_ERROR';
export const SET_DOCUMENT_MODE_REQUEST = 'SET_DOCUMENT_MODE_REQUEST';
export const SET_DOCUMENT_MODE_SUCCESS = 'SET_DOCUMENT_MODE_SUCCESS';
export const SET_DOCUMENT_MODE_ERROR = 'SET_DOCUMENT_MODE_ERROR';
export const SET_DOCUMENT_SHARE_REQUEST = 'SET_DOCUMENT_SHARE_REQUEST';
export const SET_DOCUMENT_SHARE_SUCCESS = 'SET_DOCUMENT_SHARE_SUCCESS';
export const SET_DOCUMENT_SHARE_ERROR = 'SET_DOCUMENT_SHARE_ERROR';
export const SET_DOCUMENT_NAME = 'SET_DOCUMENT_NAME';
export const SET_DOCUMENT_CONTENT = 'SET_DOCUMENT_CONTENT';
export const SET_DOCUMENT_SAVED = 'SET_DOCUMENT_SAVED';
export const RUN_DOCUMENT_REQUEST = 'RUN_DOCUMENT_REQUEST';
export const RUN_DOCUMENT_ERROR = 'RUN_DOCUMENT_ERROR';
export const SHOW_EXECUTION_RESULT = 'SHOW_EXECUTION_RESULT';

export const authInit = () => ({
    type: AUTH_INIT
});

export const signIn = (profile) => ({
    type: SING_IN,
    payload: {
        name: profile.getName(),
        email: profile.getEmail(),
        imageUrl: profile.getImageUrl()
    }
});

export const signOut = () => ({
    type: SING_OUT
});

export const fetchDocuments = () => {
    return async (dispatch) => {
        dispatch({type: FETCH_DOCUMENTS_REQUEST});

        try {
            const response = await Api.get('documents');

            if (response.ok) {
                const documents = await response.json();
                dispatch({type: FETCH_DOCUMENTS_SUCCESS, payload: documents});
            } else {
                throw new Error();
            }
        } catch (error) {
            errorMessage('Error! Something went wrong');
            dispatch({type: FETCH_DOCUMENTS_ERROR});
        }
    };
};

export const fetchDocument = (id) => {
    return async (dispatch) => {
        dispatch({type: FETCH_DOCUMENT_REQUEST});

        try {
            const [responseDocument, responseContent] = await Promise.all([
                Api.get(`documents/${id}`),
                Api.get(`documents/${id}/content`)
            ]);

            if (responseDocument.ok && responseContent.ok) {
                const document = await responseDocument.json();
                document.content = await responseContent.text();

                dispatch({
                    type: FETCH_DOCUMENT_SUCCESS,
                    payload: {document}
                });
            } else {
                throw new Error();
            }
        } catch (error) {
            errorMessage('Error! Something went wrong');
            dispatch({type: FETCH_DOCUMENT_ERROR});
        }
    };
};

export const deleteDocument = (id) => {
    return async (dispatch) => {
        try {
            const response = await Api.delete('documents', id);

            if (response.ok) {
                dispatch({type: DELETE_DOCUMENT_SUCCESS, payload: id});
            } else {
                throw new Error();
            }
        } catch (error) {
            errorMessage('Error! Something went wrong');
        }
    };
};

export const shareDocument = (id, share) => {
    return async (dispatch) => {
        dispatch({type: SET_DOCUMENT_SHARE_REQUEST, payload: id});

        try {
            const response = await Api.patch('documents', {id, share});

            if (response.ok) {
                const document = await response.json();
                dispatch({type: SET_DOCUMENT_SHARE_SUCCESS, payload: document});
            } else {
                throw new Error();
            }
        } catch (error) {
            dispatch({type: SET_DOCUMENT_SHARE_ERROR, payload: id});
            errorMessage('Error! Something went wrong');
        }
    };
};

export const createDocument = (history) => {
    return async (dispatch) => {
        dispatch({type: CREATE_DOCUMENT_REQUEST});
        try {
            const response = await Api.post('documents');
            if (response.ok) {
                dispatch({type: CREATE_DOCUMENT_SUCCESS});
                const document = await response.json();
                history.push('/' + document.id);
            } else {
                throw new Error();
            }

        } catch (error) {
            dispatch({type: CREATE_DOCUMENT_ERROR});
            errorMessage('createDocument error');
        }
    };
};

export const setDocumentMode = (id, mode) => {
    return async (dispatch) => {
        dispatch({type: SET_DOCUMENT_MODE_REQUEST});

        try {
            const response = await Api.patch('documents', {id, mode});

            if (response.ok) {
                const document = await response.json();
                dispatch({type: SET_DOCUMENT_MODE_SUCCESS, payload: document});
            } else {
                throw new Error();
            }
        } catch (error) {
            dispatch({type: SET_DOCUMENT_MODE_ERROR});
            errorMessage('Error! Something went wrong');
        }
    };
};

export const setDocumentName = (id, name) => {
    return async (dispatch) => {
        dispatch({type: SET_DOCUMENT_NAME, payload: name});

        try {
            const response = await Api.patch('documents', {id, name});

            if (!response.ok) {
                throw new Error();
            }
        } catch (error) {
            errorMessage('Error! Something went wrong');
        }
    };
};

export const runDocument = (id) => {
    return async (dispatch) => {
        dispatch({type: RUN_DOCUMENT_REQUEST});
        try {
            const response = await Api.post(`documents/${id}/run`, {});
            if (!response.ok) {
                throw new Error();
            }
        } catch (error) {
            dispatch({type: RUN_DOCUMENT_ERROR});
            errorMessage('Run Document error');
        }
    };
};

export const showExecutionResult = (result) => {
    return {type: SHOW_EXECUTION_RESULT, payload: result};
};

export const setDocumentContent = (content) => {
    return {type: SET_DOCUMENT_CONTENT, payload: content};
};

export const setDocumentSaved = (saved) => {
    return {type: SET_DOCUMENT_SAVED, payload: saved};
};