import {message} from 'antd';

export const errorMessage = (content = 'Error') => {
    message.error({
        content,
        style: {marginTop: '10px'}
    });
};