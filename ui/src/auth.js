import {authInit, signIn, signOut} from './redux/Actions';
import Api from "./api";
import Log from "./log";

class Auth {
    static _store = null;
    static _auth = null;
    static _initParams = {client_id: process.env.REACT_APP_AUTH_CLIENT_ID};

    static init = (store) => {
        this._store = store;
        window.gapi.load('auth2', this.#loadHandler);
    };

    static getAccessToken = () => {
        return this._auth?.isSignedIn.get() ?
            this._auth.currentUser.get().getAuthResponse(true).access_token :
            null;
    };

    static signOut = () => {
        return this._auth?.signOut();
    };

    static signIn = () => {
        return this._auth?.signIn();
    };

    static _signInHandler = () => {
        try {
            Api.post('users/signin');
        } catch (e) {
            Log.info(`SignIn error. ${e}`);
        }

        const profile = this._auth.currentUser.get().getBasicProfile();
        this._store.dispatch(signIn(profile));
    };

    static #loadHandler = async () => {
        this._auth = await window.gapi.auth2.init(this._initParams);

        this._store.dispatch(authInit());

        if (this._auth.isSignedIn.get()) {
            this._signInHandler();
        }

        this._auth.isSignedIn.listen(signedIn => {
            if (signedIn) {
                this._signInHandler()
            } else {
                this._store.dispatch(signOut());
            }
        });
    };
}

export default Auth;