import Auth from './auth';
import Log from './log';

class Api {
    static _baseUrl = process.env.REACT_APP_REST_URL;

    static getHeaders() {
        const token = Auth.getAccessToken();

        return {
            'Authorization': token ? 'Bearer ' + token : null,
            'Content-Type': 'application/json'
        };
    }

    static logError = (method, url, error) => {
        Log.error(`${method} ${url} ${error}`)
    };

    static logResponse(method, url, response) {
        if (!response.ok) {
            Log.error(`${method} ${url} ${response.status}`);
        }
        return response;
    }

    static withLog = (url, options) => {
        return fetch(url, options)
            .then(r => this.logResponse(options.method, url, r))
            .catch(e => this.logError(options.method, url, e));
    };

    static post = (url, data) => {
        const options = {
            method: 'POST',
            headers: this.getHeaders(),
            body: JSON.stringify(data)
        };

        return this.withLog(`${this._baseUrl}/${url}`, options);
    };

    static get = (url) => {
        const options = {
            method: 'GET',
            headers: this.getHeaders()
        };

        return this.withLog(`${this._baseUrl}/${url}`, options);
    };

    static delete = (url, id) => {
        const options = {
            method: 'DELETE',
            headers: this.getHeaders()
        };

        return this.withLog(`${this._baseUrl}/${url}/${id}`, options);
    };

    static patch = (url, data) => {
        const options = {
            method: 'PATCH',
            headers: this.getHeaders(),
            body: JSON.stringify(data)
        };

        return this.withLog(`${this._baseUrl}/${url}`, options);
    };
}

export default Api;