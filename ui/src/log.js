class Log {
    static error(text, data) {
        window._LTracker.push({
            text,
            level: 'error',
            data
        });
    }

    static info(text) {
        window._LTracker.push({
            text: text,
            level: 'info'
        });
    }
}

export default Log;