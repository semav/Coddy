import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import Auth from './auth';
import {applyMiddleware, createStore} from 'redux';
import {Provider} from 'react-redux';
import thunk from 'redux-thunk';
import rootReducer from './redux/Reducers';
import 'antd/dist/antd.css';
import {event, initialize} from 'react-ga';

initialize('UA-154744092-1');

event({category: 'User', action: 'initialize'});

const store = createStore(rootReducer, applyMiddleware(thunk));
Auth.init(store);

ReactDOM.render(
    <React.StrictMode>
        <Provider store={store}>
            <App/>
        </Provider>
    </React.StrictMode>,
    document.getElementById('root')
);