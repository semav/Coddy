import React from 'react';
import {BrowserRouter, Route, Switch} from 'react-router-dom';
import MainPage from './component/main-page';
import DocumentsPage from './component/documents-page';
import DocumentPage from './component/document-page/DocumentPage';
import './App.css';

function App() {
    return (
        <BrowserRouter>
            <Switch>
                <Route exact path="/">
                    <MainPage/>
                </Route>
                <Route exact path="/documents/all" strict>
                    <DocumentsPage/>
                </Route>
                <Route exact path="/:documentId" strict>
                    <DocumentPage/>
                </Route>
            </Switch>
        </BrowserRouter>
    );
}

export default App;
