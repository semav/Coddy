import React from 'react';
import {Col, Layout, Row} from 'antd';
import Logo from '../logo';
import UserAvatar from '../user-avatar';
import css from './Layout.module.css'

const {Header, Content} = Layout;

const FullHeightLayout = (props) => {
    return (
        <Layout className={css.fullHeight}>
            <Header>
                <Row justify="space-between">
                    <Col>
                        <Logo/>
                    </Col>
                    <Col>
                        <UserAvatar/>
                    </Col>
                </Row>
            </Header>
            <Content className={css.content}>
                {props.children}
            </Content>
        </Layout>
    );
}

export default FullHeightLayout;