import React from 'react'
import {Link} from 'react-router-dom';
import css from './Logo.module.css'

const Logo = () => {
    return (
        <Link to="/">
            <img src={'/logo.png'} alt="Logo" className={css.logoImg}/>
            <span className={css.logoText}>Coddy</span>
        </Link>
    );
}

export default Logo;