import React, {Component} from 'react';
import {Row} from 'antd';
import css from './DocumentPage.module.css'
import {connect} from 'react-redux';

class Output extends Component {

    componentDidMount() {
        window.dispatchEvent(new Event('resize'));
    }

    render() {
        return <Row className={css.output}>{this.props.output}</Row>;
    }
}

const mapStateToProps = (state) => {
    return {
        output: state.documentPage.output
    };
};

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(Output);