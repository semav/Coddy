import React from 'react';
import {withRouter} from 'react-router-dom';
import {Col, Row} from 'antd';
import afterAuthInit from '../hoc/after-auth-init';
import Document from '../document/Document';
import {connect} from 'react-redux';
import FullHeightLayout from '../layout/FullHeightLayout';
import css from './DocumentPage.module.css'
import DocumentOptions from './DocumentOptions';
import Output from './Output';

const title = (document) => document ? `${document?.name} - Coddy` : 'Coddy';

const DocumentPage = ({match, document, outputVisible}) => {
    window.document.title = title(document);
    return (
        <FullHeightLayout>
            <div className={css.container}>
                {document && <DocumentOptions/>}
                <Row className={css.containerRow}>
                    <Col xs={24}>
                        <Document documentId={match.params.documentId}/>
                    </Col>
                </Row>
                {outputVisible && <Output/>}
            </div>
        </FullHeightLayout>
    );
};

const mapStateToProps = (state) => {
    return {
        document: state.documentPage.document,
        outputVisible: state.documentPage.outputVisible
    };
};

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(afterAuthInit(withRouter(DocumentPage)));