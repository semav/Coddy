import React, {useState} from 'react';
import {Button, Col, Input, Popover, Row, Select, Space, Tag} from 'antd';
import css from './DocumentPage.module.css'
import {connect} from 'react-redux';
import {CopyOutlined, PlayCircleOutlined, ShareAltOutlined} from '@ant-design/icons';
import {runDocument, setDocumentMode, setDocumentName} from '../../redux/Actions';
import MyDocumentsButton from '../my-documents-button';

const {Option} = Select;

const DocumentOptions = ({document, user, documentModeLoading, running, setMode, setName, run}) => {

    const [renaming, setRenaming] = useState(false);
    const [newName, setNewName] = useState(null);

    const onRun = () => {
        run(document.id);
    };

    const onChangeMode = (value) => {
        setMode(document.id, value);
    };

    const startRenaming = () => {
        setRenaming(true);
        setNewName(document?.name);
    };

    const cancelRenaming = () => {
        setRenaming(false);
    };

    const onChangeName = (e) => {
        setNewName(e.target.value);
    };

    const saveNewName = () => {
        setName(document.id, newName);
        setRenaming(false);
    };

    const header = (
        <Col>
            {
                !renaming &&
                <span className={css.header} onClick={startRenaming}>
                    {document?.name}
                </span>
            }
            {
                renaming && <Input
                    style={{width: 300}}
                    value={newName}
                    onChange={onChangeName}
                    onPressEnter={saveNewName}
                    autoFocus
                    onKeyDown={event => {
                        if (event.keyCode === 27) cancelRenaming()
                    }}
                    onBlur={saveNewName}
                />
            }
        </Col>
    );

    const copyLink = () => {
        const element = window.document.createElement('textarea');
        element.value = window.location.href;
        window.document.body.appendChild(element);
        element.select();
        window.document.execCommand('copy');
        window.document.body.removeChild(element);
    };

    const content = (
        <div style={{width: 350}}>
            <p>Use this link to share your code with the other person</p>
            <p><Input addonAfter={<Button icon={<CopyOutlined onClick={copyLink}/>} size='small'/>}
                      defaultValue={window.location.href}/></p>
        </div>
    );

    const actions = (
        <Col>
            <Space>
                {
                    document.saved && <Tag color="green">saved</Tag>
                }
                {
                    (document.mode !== 'text') &&
                    <Button onClick={onRun} type="primary" loading={running}>
                        <PlayCircleOutlined/> Run
                    </Button>
                }
                {
                    user && <MyDocumentsButton type="primary"/>
                }
                <Select loading={documentModeLoading} value={document.mode} className={css.documentMode}
                        onChange={onChangeMode}>
                    <Option value="java">Java</Option>
                    <Option value="python">Python</Option>
                    <Option value="text">Text</Option>
                    <Option value="javascript">JavaScript</Option>
                    <Option value="typescript">TypeScript</Option>
                </Select>
                {
                    document.share && <Popover content={content} trigger="click">
                        <Button>
                            <ShareAltOutlined/> Share
                        </Button>
                    </Popover>
                }
            </Space>
        </Col>
    );

    return (
        <Row className={css.optionsRow} justify="space-between">
            {header}
            {actions}
        </Row>
    );
};

const mapStateToProps = (state) => {
    return {
        user: state.user,
        document: state.documentPage.document,
        running: state.documentPage.running,
        documentModeLoading: state.documentPage.documentModeLoading
    };
};

const mapDispatchToProps = {
    setMode: setDocumentMode,
    setName: setDocumentName,
    run: runDocument
};

export default connect(mapStateToProps, mapDispatchToProps)(DocumentOptions);