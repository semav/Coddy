import React, {Component} from 'react';
import {Col, Row, Spin} from 'antd';
import {connect} from 'react-redux';
import {fetchDocument} from '../../redux/Actions';
import Editor from './Editor';

import css from '../document-page/DocumentPage.module.css';

class Document extends Component {
    componentDidMount() {
        this.props.fetchDocument(this.props.documentId);
    }

    render() {
        const {loading, document} = this.props;

        if (loading) {
            return (
                <Row justify='center' className={css.loadingRow}>
                    <Col>
                        <Spin/>
                    </Col>
                </Row>
            );
        } else if (document) {
            return <Editor/>;
        }
        return null;
    }
}

const mapStateToProps = (state) => {
    return {
        document: state.documentPage.document,
        loading: state.documentPage.loading
    };
};

const mapDispatchToProps = {
    fetchDocument
};

export default connect(mapStateToProps, mapDispatchToProps)(Document);