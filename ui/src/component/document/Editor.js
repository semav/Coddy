import React, {Component} from 'react';
import {connect} from 'react-redux';
import {fetchDocument, setDocumentContent, setDocumentSaved, showExecutionResult} from '../../redux/Actions';
import AceEditor from 'react-ace';
import DocumentProcessor from './DocumentProcessor';

import 'ace-builds/webpack-resolver'
import 'ace-builds/src-noconflict/ext-language_tools'
import 'ace-builds/src-noconflict/theme-cobalt';
import 'ace-builds/src-noconflict/theme-chrome';

class Editor extends Component {
    componentDidMount() {
        const {document} = this.props;
        this.documentProcessor = new DocumentProcessor(
            document,
            {
                changeCallback: content => this.props.setDocumentContent(content),
                errorCallback: () => this.props.fetchDocument(document.id),
                savedCallback: saved => this.props.setDocumentSaved(saved),
                executorCallback: result => this.props.showExecutionResult(result)
            });
    }

    componentWillUnmount() {
        this.documentProcessor.close();
    }

    changeHandler = (newValue) => {
        this.props.setDocumentContent(newValue);
        this.documentProcessor.setValue(newValue);
    };

    render() {
        const {document} = this.props;

        return <AceEditor
            width='100%'
            height='100%'
            mode={document.mode}
            theme="chrome"
            focus={true}
            onChange={this.changeHandler}
            value={document.content}
            enableBasicAutocompletion={true}
            enableLiveAutocompletion={true}
            enableSnippets={true}
        />;
    }
}

const mapStateToProps = (state) => {
    return {
        document: state.documentPage.document
    };
};

const mapDispatchToProps = {
    setDocumentContent,
    setDocumentSaved,
    fetchDocument,
    showExecutionResult
};

export default connect(mapStateToProps, mapDispatchToProps)(Editor);