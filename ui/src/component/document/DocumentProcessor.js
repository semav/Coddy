import {Client} from '@stomp/stompjs';
import DiffMatchPatch from 'diff-match-patch';
import {uuid} from 'uuidv4';

const DOCUMENT_MAX_LENGTH = 100000;
const PUBLISH_DELAY = 300;

export default class DocumentProcessor {

    constructor(document, callbacks) {
        this.documentId = document.id;

        this.changeCallback = callbacks.changeCallback;
        this.errorCallback = callbacks.errorCallback;
        this.savedCallback = callbacks.savedCallback;
        this.executorCallback = callbacks.executorCallback;

        this.client = new Client({splitLargeFrames: true});
        this.diffMatchPatch = new DiffMatchPatch();
        this.timer = null;

        this.sentPatches = [];
        this.mutualValue = document.content;
        this.lastValue = document.content;
        this.newValue = null;
        this.isEditing = false;

        this.client.configure({
            brokerURL: process.env.REACT_APP_WS_URL,
            onConnect: this.connectHandler,
            reconnectDelay: 0,
            onWebSocketClose: this.webSocketErrorHandler

        });
        this.client.activate();
    }

    webSocketErrorHandler = () => {
        if (this.client.connected) {
            this.errorCallback();
        }
    };

    setValue(newValue) {
        if (newValue.length > DOCUMENT_MAX_LENGTH) {
            newValue = newValue.substring(0, DOCUMENT_MAX_LENGTH);
            this.changeCallback(newValue);
        }

        this.isEditing = true;
        clearTimeout(this.timer);
        this.newValue = newValue;
        this.timer = setTimeout(this.publishChange, PUBLISH_DELAY);
        this.savedCallback(false);
    }

    connectHandler = () => {
        const destination = `/topic/documents/${this.documentId}/patch`;
        const destinationExecutor = `/topic/documents/${this.documentId}/executor`;
        this.client.subscribe(destination, this.receiveHandler);
        this.client.subscribe(destinationExecutor, this.receiveExecutionResult);
    };

    receiveExecutionResult = message => {
        const result = JSON.parse(message.body);
        this.executorCallback(result);
    };

    receiveHandler = message => {
        if (this.isEditing) {
            clearTimeout(this.timer);
            this.publishChange();
        }

        const patch = this.diffMatchPatch.patch_fromText(message.body);
        this.mutualValue = this.diffMatchPatch.patch_apply(patch, this.mutualValue)[0];

        if (this.sentPatches.length === 0) {
            this.lastValue = this.mutualValue;
            this.changeCallback(this.mutualValue);
        } else {
            const {patchId} = message.headers;
            const firstSentPatch = this.sentPatches[0];

            if (firstSentPatch.patchId === patchId) {
                this.sentPatches.shift();
            }

            this.lastValue = this.mutualValue;
            for (let i = 0; i < this.sentPatches.length; i++) {
                this.lastValue = this.diffMatchPatch.patch_apply(this.sentPatches[i].patch, this.lastValue)[0];
            }
            this.changeCallback(this.lastValue);
        }

        if (this.sentPatches.length === 0) {
            this.savedCallback(true);
        }
    };

    publishChange = () => {
        const patch = this.diffMatchPatch.patch_make(this.lastValue, this.newValue);
        const patchId = uuid();

        this.isEditing = false;
        this.lastValue = this.newValue;
        this.publishPatch({
            patchId: patchId,
            patch: patch
        });
    };

    publishPatch = (patch) => {
        const documentId = this.documentId;
        const patchText = this.diffMatchPatch.patch_toText(patch.patch);

        if (patchText == null || patchText.length === 0)
            return;

        const params = {
            destination: `/app/documents/${documentId}/patch`,
            body: patchText,
            headers: {patchId: patch.patchId}
        };

        this.sentPatches.push(patch);
        this.client.publish(params);
    };

    close() {
        this.client.deactivate();
        clearTimeout(this.timer);
    }
}