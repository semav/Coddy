import React from 'react';
import {Col, Row, Space} from 'antd';
import SignIn from '../sign-in';
import css from './MainPage.module.css'
import MainLayout from '../layout/MainLayout';
import CreateDocumentButton from '../create-document-button/CreateDocumentButton';
import {connect} from 'react-redux';
import MyDocumentsButton from '../my-documents-button';

const MainPage = ({user}) => {
    window.document.title = 'Coddy';

    return (
        <MainLayout>
            <Row justify="center">
                <Col xs={22} sm={18} lg={12} className={css.title}>
                    Real-time collaborative coding.
                </Col>
            </Row>
            <Row justify="center">
                <Col xs={22} sm={18} lg={12} className={css.description}>
                    Create the code together with other users in real time and run the code online.
                    Conduct technical interviews, do pair programming or teach to code.
                </Col>
            </Row>
            <Row justify="center">
                <Col xs={22} sm={18} lg={12} className={css.description}>
                    <Space>
                        <CreateDocumentButton size='large'/>
                        {user && <MyDocumentsButton size='large'/>}
                        <SignIn/>
                    </Space>
                </Col>
            </Row>
        </MainLayout>
    );
};

const mapStateToProps = (state) => {
    return {user: state.user};
};

export default connect(mapStateToProps, null)(MainPage);