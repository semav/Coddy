import React from 'react';
import DocumentsList from '../documents-list';
import {Breadcrumb, Col, Row} from 'antd';
import css from './DocumentsPage.module.css'
import {Link} from 'react-router-dom';
import MainLayout from '../layout/MainLayout';
import CreateDocumentButton from '../create-document-button/CreateDocumentButton';
import afterAuth from '../hoc/after-auth-init';
import withAuth from '../hoc/with-signed-in-user';

const DocumentsPage = () => {
    document.title = 'My documents - Coddy';

    return (
        <MainLayout>
            <Row justify="center" className={css.breadcrumbRow}>
                <Col xs={22} sm={14} lg={8}>
                    <Breadcrumb>
                        <Breadcrumb.Item><Link to={'/'}>Home</Link></Breadcrumb.Item>
                        <Breadcrumb.Item>Documents</Breadcrumb.Item>
                    </Breadcrumb>
                </Col>
            </Row>
            <Row>
                <Col xs={22} sm={14} lg={8}/>
                <CreateDocumentButton label='Create'/>
                <Col/>
            </Row>
            <Row justify="center">
                <Col xs={22} sm={14} lg={8}>
                    <DocumentsList/>
                </Col>
            </Row>
        </MainLayout>
    );
};

export default afterAuth(withAuth(DocumentsPage));