import React from 'react'
import {Button} from 'antd';
import {GoogleCircleFilled} from '@ant-design/icons';
import {connect} from 'react-redux';
import Auth from '../../auth';
import afterAuthInit from '../hoc/after-auth-init';

const SignIn = ({user}) => {
    return (
        !user && <Button size='large' onClick={() => Auth.signIn()}>
            <GoogleCircleFilled/> Sign in with Google
        </Button>
    )
}

const mapStateToProps = (state) => {
    return {user: state.user};
};

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(afterAuthInit(SignIn));
