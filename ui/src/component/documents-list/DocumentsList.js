import React, {Component} from 'react';
import {connect} from 'react-redux';
import {List} from 'antd';
import {deleteDocument, fetchDocuments, shareDocument} from '../../redux/Actions'
import DocumentsListItem from './DocumentsListItem';

class DocumentsList extends Component {

    componentDidMount() {
        this.props.fetchDocuments();
    }

    render() {
        const {loading, documents} = this.props;
        const handleDelete = id => this.props.deleteDocument(id);
        const handleShare = (id, share) => this.props.shareDocument(id, share);

        const renderItem = (item) => {
            return <DocumentsListItem {...item} onDelete={handleDelete} onShare={handleShare}/>;
        };

        return (
            <List
                rowKey='id'
                loading={loading}
                dataSource={documents}
                renderItem={renderItem}
            />
        );
    }
}

const mapStateToProps = (state) => {
    return {
        loading: state.documentsList.loading,
        documents: state.documentsList.documents
    };
};

const mapDispatchToProps = {
    fetchDocuments,
    deleteDocument,
    shareDocument
};

export default connect(mapStateToProps, mapDispatchToProps)(DocumentsList);