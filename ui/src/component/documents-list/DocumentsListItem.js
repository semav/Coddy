import React from 'react';
import {DeleteOutlined} from '@ant-design/icons';
import {Button, List, Popconfirm, Switch, Tag} from 'antd';
import {Link} from 'react-router-dom';

const DocumentsListItem = ({id, lastUpdate, name, mode, share, shareLoading, onDelete, onShare}) => {
    const date = new Date(lastUpdate);

    const handleDelete = () => {
        onDelete(id);
    };

    const handleShare = () => {
        onShare(id, !share);
    };

    const deleteButton = <Popconfirm
        title={`Delete the document "${name}"`}
        onConfirm={handleDelete}
        okText="Yes"
        cancelText="No"
    >
        <Button
            id={id}
            size='small'
            icon={<DeleteOutlined/>}
            danger>
            Delete
        </Button>
    </Popconfirm>

    const title = <Link to={`/${id}`}>{name}</Link>;

    return (
        <List.Item actions={[deleteButton]}>
            <List.Item.Meta title={title} description={date.toLocaleTimeString() + ' ' + date.toLocaleDateString()}/>
            <Tag color="geekblue">{mode}</Tag>
            <Switch title='Share' checkedChildren='Share' unCheckedChildren='Share' checked={share}
                    loading={shareLoading} onClick={handleShare}/>
        </List.Item>
    )
};

export default DocumentsListItem;