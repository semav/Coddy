import React from 'react';
import {Button} from 'antd';
import {Link} from 'react-router-dom';

const MyDocumentsButton = ({size, type}) =>
    <Link to='/documents/all'>
        <Button size={size} type={type}>
            My documents
        </Button>
    </Link>;


export default MyDocumentsButton;