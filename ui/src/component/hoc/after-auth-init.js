import React from 'react';
import {connect} from 'react-redux';
import {compose} from 'redux'

const afterAuthInit = Component => (props) => {
    const {authInit, dispatch, ...rest} = props;

    if (authInit)
        return <Component {...rest}/>;
    else
        return null;
}

const mapStateToProps = (state) => {
    return {authInit: state.authInit};
};

export default compose(connect(mapStateToProps, null), afterAuthInit);