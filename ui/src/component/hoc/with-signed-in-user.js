import React from 'react';
import {connect} from 'react-redux';
import {compose} from 'redux'
import SignIn from '../sign-in/SignIn';
import {Alert, Col, Row} from 'antd';
import MainLayout from '../layout/MainLayout';
import css from './WithSignedInUser.module.css'

const withSignedIsUser = Component => (props) => {
    const {user, dispatch, ...rest} = props;

    if (user)
        return <Component {...rest}/>;
    else
        return (
            <MainLayout>
                <Row justify="center" className={css.warningRow}>
                    <Col xs={22} sm={14} lg={8}>
                        <Alert message="Authentication is required"
                               description="You need to sign in into your account"
                               type="warning"
                               showIcon
                        />
                    </Col>
                </Row>
                <Row justify='center'>
                    <Col xs={22} sm={14} lg={8}>
                        <SignIn/>
                    </Col>
                </Row>
            </MainLayout>
        );
}

const mapStateToProps = (state) => {
    return {user: state.user};
};

export default compose(connect(mapStateToProps, null), withSignedIsUser);