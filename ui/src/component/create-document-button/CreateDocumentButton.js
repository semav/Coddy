import React from 'react';
import {Button} from 'antd';
import {createDocument} from '../../redux/Actions';
import {connect} from 'react-redux';
import {withRouter} from 'react-router-dom';

const CreateDocumentButton = ({label = 'New document', size = 'middle', loading, create, history}) =>
    <Button size={size} type="primary" loading={loading} onClick={() => create(history)}>
        {label}
    </Button>;

const mapStateToProps = (state) => {
    return {
        loading: state.createDocumentLoading
    };
};

const mapDispatchToProps = {
    create: createDocument
};

export default connect(mapStateToProps, mapDispatchToProps)(withRouter(CreateDocumentButton));