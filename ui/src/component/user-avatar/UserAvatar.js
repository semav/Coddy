import React from 'react';
import {connect} from 'react-redux';
import {Avatar, Button, Popover} from 'antd';
import {Link} from 'react-router-dom';
import Auth from '../../auth';

const UserAvatar = ({user}) => {

    const content = (
        <>
            <Link to='/documents/all'>
                <Button type="link">
                    My documents
                </Button>
            </Link>
            <br/>
            <Button type="link" danger onClick={() => Auth.signOut()}>
                Sign out
            </Button>
        </>
    );

    return (
        user && (
            <Popover placement="bottomRight" content={content} title={user.name} trigger="click">
                <Avatar size="large" src={user.imageUrl}/>
            </Popover>
        )
    )
};

const mapStateToProps = (state) => {
    return {
        user: state.user
    };
};

const mapDispatchToProps = {};

export default connect(mapStateToProps, mapDispatchToProps)(UserAvatar);