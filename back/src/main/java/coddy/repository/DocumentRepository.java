package coddy.repository;

import coddy.entity.Document;
import coddy.entity.DocumentView;
import coddy.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import java.util.List;
import java.util.Optional;

public interface DocumentRepository extends JpaRepository<Document, String> {
    DocumentView getById(String id);
    List<DocumentView> findByUserOrderByLastUpdateDesc(User user);

    @Query(nativeQuery = true, value =
            "with numbers as ( " +
            "select cast(substring(d.name, 'Document ([0-9]+)') as INTEGER) as n " +
            "    from \"document\" d " +
            "    where substring(d.name, 'Document ([0-9]+)') is not null and d.email = :email" +
            ") " +
            "select n + 1 as nextNumber " +
            "from numbers " +
            "where n + 1 not in (select n from numbers) " +
            "limit 1;")
    Optional<Integer> findNexNumber(@Param("email") String email);
}
