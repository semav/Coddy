package coddy.dto.messsage;

import lombok.AccessLevel;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;

@Getter
@Setter
@ToString
@EqualsAndHashCode
@Accessors(chain = true)
@FieldDefaults(level = AccessLevel.PRIVATE)
public class TaskResultDto {
    String documentId;
    String message;
    Status status;

    public enum Status {
        STARTED,
        IN_PROGRESS,
        COMPLETED,
        ERROR
    }
}
