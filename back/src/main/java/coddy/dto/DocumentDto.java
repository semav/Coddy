package coddy.dto;

import coddy.service.DocumentAccess;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;
import java.time.Instant;

@Getter
@Setter
@Accessors(chain = true)
@FieldDefaults(level = AccessLevel.PRIVATE)
public class DocumentDto {
    String id;
    String name;
    String mode;
    Instant lastUpdate;
    Boolean share;
    UserDto user;
    DocumentAccess access;
}
