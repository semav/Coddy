package coddy.service;

import coddy.dto.messsage.TaskDto;
import coddy.dto.messsage.TaskResultDto;
import coddy.messaging.WsTaskResultProducer;
import com.google.common.cache.RemovalListener;
import com.google.common.cache.RemovalNotification;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class TaskRemovalListener implements RemovalListener<String, TaskDto> {
    private final WsTaskResultProducer wsTaskResultProducer;

    @Override
    public void onRemoval(RemovalNotification<String, TaskDto> removalNotification) {
        log.debug("Removing task id - {}, cause - {} ", removalNotification.getKey(), removalNotification.getCause());

        if (removalNotification.wasEvicted()) {
            String id = removalNotification.getKey();

            TaskResultDto taskResultDto = new TaskResultDto()
                    .setDocumentId(id)
                    .setStatus(TaskResultDto.Status.ERROR)
                    .setMessage("Execution took too long");

            wsTaskResultProducer.send(taskResultDto);
        }
    }
}
