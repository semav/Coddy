package coddy.service;

import coddy.entity.CacheableDocument;
import coddy.messaging.WsPatchProducer;
import com.google.common.cache.LoadingCache;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import java.time.Instant;

@Service
@RequiredArgsConstructor
public class DocumentContentService {
    private final LoadingCache<String, CacheableDocument> documentsCache;
    private final WsPatchProducer wsPatchProducer;
    private final DocumentPersistService documentPersistService;

    @PreAuthorize("@accessControl.hasPermission(#id, 'SHARED')")
    public CacheableDocument get(String id) {
        return documentsCache.getUnchecked(id);
    }

    public void applyPatches(String id, String patch, String patchId) {
        documentsCache.getUnchecked(id);
        documentsCache.asMap().computeIfPresent(id, (key, value) -> {
            CacheableDocument document = value.applyPatch(patch);
            documentPersistService.persist(document, Instant.now());
            wsPatchProducer.send(id, patch, patchId);
            return document;
        });
    }
}