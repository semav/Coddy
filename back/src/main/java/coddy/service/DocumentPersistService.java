package coddy.service;

import coddy.entity.CacheableDocument;
import coddy.repository.DocumentRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.function.Consumer;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class DocumentPersistService {
    private static final int CAPACITY = 10_000;
    private final DocumentRepository documentRepository;

    private Thread thread;
    private final BlockingQueue<DocumentWithDateTime> queue = new ArrayBlockingQueue<>(CAPACITY);

    @PostConstruct
    public void runPersistThread() {
        Runnable runnable = () -> {
            do {
                try {
                    Collection<DocumentWithDateTime> documents = new ArrayList<>();
                    documents.add(queue.take());
                    queue.drainTo(documents);
                    persist(documents);
                } catch (InterruptedException e) {
                    log.info("Persisting documents has stopped");
                    Thread.currentThread().interrupt();
                    return;
                } catch (Exception e) {
                    log.error("Persisting error", e);
                }
            } while (!Thread.interrupted());
        };

        thread = new Thread(runnable);
        thread.start();
    }

    @PreDestroy
    public void stopPersistThread() throws InterruptedException {
        log.info("Stopping persisting documents");
        thread.interrupt();
        thread.join(1000);
    }

    void persist(CacheableDocument cacheableDocument, Instant instant) {
        queue.add(new DocumentWithDateTime(cacheableDocument, instant));
    }

    private void persist(Collection<DocumentWithDateTime> documents) {
        long start = System.nanoTime();

        Consumer<DocumentWithDateTime> saveDocument = doc -> {
            documentRepository
                    .findById(doc.getId())
                    .ifPresent(document -> {
                        document.setLastUpdate(doc.instant);
                        document.setContent(doc.cacheableDocument.getValue());
                        documentRepository.save(document);
                    });
        };

        documents.stream()
                .collect(Collectors.groupingBy(DocumentWithDateTime::getId))
                .forEach((id, list) -> list
                        .stream()
                        .max(Comparator.comparing(DocumentWithDateTime::getOrder))
                        .ifPresent(saveDocument)
                );

        long end = System.nanoTime();
        log.info("Persisting of {} documents took {} ms",
                documents.size(), TimeUnit.NANOSECONDS.toMillis(end - start));
    }

    private static final class DocumentWithDateTime {
        private final CacheableDocument cacheableDocument;
        private final Instant instant;

        DocumentWithDateTime(CacheableDocument cacheableDocument, Instant instant) {
            this.cacheableDocument = cacheableDocument;
            this.instant = instant;
        }

        public String getId() {
            return cacheableDocument.getId();
        }

        long getOrder() {
            return cacheableDocument.getOrder();
        }
    }
}