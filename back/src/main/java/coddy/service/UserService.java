package coddy.service;

import coddy.entity.User;
import coddy.entity.UserPrincipal;
import coddy.repository.UserRepository;
import org.springframework.stereotype.Service;
import java.time.Instant;

@Service
public class UserService {

    private final UserRepository userRepository;
    private final AuthenticationService authenticationService;

    public UserService(UserRepository userRepository, AuthenticationService authenticationService) {
        this.userRepository = userRepository;
        this.authenticationService = authenticationService;
    }

    public User signIn() {
        UserPrincipal userPrincipal = authenticationService.getUserPrincipal();
        User user = new User();
        user.setName(userPrincipal.getName());
        user.setEmail(userPrincipal.getEmail());
        user.setLastSignIn(Instant.now());

        userRepository.save(user);

        return user;
    }
}
