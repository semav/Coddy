package coddy.service;

import coddy.entity.DocumentView;
import coddy.entity.UserPrincipal;
import coddy.repository.DocumentRepository;
import org.springframework.stereotype.Service;

@Service
public class AccessControl {
    private final AuthenticationService authenticationService;
    private final DocumentRepository documentRepository;

    public AccessControl(
            DocumentRepository documentRepository,
            AuthenticationService authenticationService) {
        this.documentRepository = documentRepository;
        this.authenticationService = authenticationService;
    }

    public boolean hasPermission(String id, DocumentAccess requiredAccess) {
        DocumentView documentView = documentRepository.getById(id);

        if (documentView == null) {
            return true;
        }

        DocumentAccess documentAccess = getAccess(documentView);

        return DocumentAccess.FULL.equals(documentAccess) || requiredAccess.equals(documentAccess);
    }

    public DocumentAccess getAccess(String id) {
        DocumentView documentView = documentRepository.getById(id);

        if (documentView == null) {
            return null;
        }

        return getAccess(documentView);
    }

    private DocumentAccess getAccess(DocumentView documentView) {
        UserPrincipal userPrincipal = authenticationService.getUserPrincipal();
        String userEmail = userPrincipal == null ? null : userPrincipal.getEmail();

        if (documentView.getUser() == null) {
            return DocumentAccess.SHARED;
        }
        else if (documentView.getUser().getEmail().equals(userEmail)) {
            return DocumentAccess.FULL;
        }
        else if (Boolean.TRUE.equals(documentView.getShare())) {
            return DocumentAccess.SHARED;
        }
        return null;
    }
}
