package coddy.service;

import coddy.entity.Document;
import coddy.entity.DocumentView;
import coddy.entity.User;
import coddy.entity.UserPrincipal;
import coddy.repository.DocumentRepository;
import coddy.repository.UserRepository;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class DocumentService {
    private static final int DOCUMENT_ID_LENGTH = 10;
    private static final String DOCUMENT_DEFAULT_NAME = "Document";
    private static final int DOCUMENT_FIRST_NUMBER = 1;

    private final DocumentRepository documentRepository;
    private final AuthenticationService authenticationService;
    private final UserRepository userRepository;

    DocumentService(DocumentRepository documentRepository, AuthenticationService authenticationService, UserRepository userRepository) {
        this.documentRepository = documentRepository;
        this.authenticationService = authenticationService;
        this.userRepository = userRepository;
    }

    @Transactional(readOnly = true)
    @PreAuthorize("@accessControl.hasPermission(#id, 'SHARED')")
    public DocumentView get(String id) {
        return documentRepository.getById(id);
    }

    @Transactional(readOnly = true)
    public List<DocumentView> getList() {
        UserPrincipal userPrincipal = authenticationService.getUserPrincipal();
        User user = userRepository.getOne(userPrincipal.getEmail());
        return documentRepository.findByUserOrderByLastUpdateDesc(user);
    }

    public DocumentView create() {
        UserPrincipal userPrincipal = authenticationService.getUserPrincipal();
        String id = RandomStringUtils.randomAlphanumeric(DOCUMENT_ID_LENGTH).toLowerCase();
        String name = getDefaultDocumentName(userPrincipal);
        User user = null;

        if (userPrincipal != null) {
            user = userRepository.findById(userPrincipal.getEmail()).orElse(null);
        }

        Document document = new Document(id, name, user);

        return documentRepository.save(document);
    }

    @PreAuthorize("@accessControl.hasPermission(#id, 'SHARED')")
    public Document update(String id, Document newDocument) {
        Optional<Document> optionalDocument = documentRepository.findById(id);

        if (!optionalDocument.isPresent())
            return null;

        Document document = optionalDocument.get();

        if (newDocument.getShare() != null) {
            document.setShare(newDocument.getShare());
        }

        if (newDocument.getName() != null) {
            document.setName(newDocument.getName());
        }

        if (newDocument.getMode() != null) {
            document.setMode(newDocument.getMode());
        }

        documentRepository.save(document);

        return document;
    }

    @PreAuthorize("@accessControl.hasPermission(#id, 'FULL')")
    public void delete(String id) {
        documentRepository.deleteById(id);
    }

    private String getDefaultDocumentName(UserPrincipal userPrincipal) {
        if (userPrincipal == null || userPrincipal.getEmail() == null) {
            return DOCUMENT_DEFAULT_NAME;
        }

        Optional<Integer> number = documentRepository.findNexNumber(userPrincipal.getEmail());
        return DOCUMENT_DEFAULT_NAME + " " + number.orElse(DOCUMENT_FIRST_NUMBER);
    }
}