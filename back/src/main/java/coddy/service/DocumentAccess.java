package coddy.service;

public enum DocumentAccess {
    FULL,
    SHARED
}
