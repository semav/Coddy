package coddy.service;

import coddy.entity.CacheableDocument;
import coddy.repository.DocumentRepository;
import com.google.common.cache.CacheLoader;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CacheableDocumentLoader extends CacheLoader<String, CacheableDocument> {
    private final DocumentRepository documentRepository;

    @Override
    public CacheableDocument load(String id) {
        return documentRepository
                .findById(id)
                .map(CacheableDocument::new)
                .orElse(null);
    }
}
