package coddy.service;

import coddy.dto.messsage.TaskDto;
import coddy.dto.messsage.TaskResultDto;
import coddy.entity.CacheableDocument;
import coddy.entity.DocumentView;
import coddy.messaging.TaskProducer;
import coddy.messaging.WsTaskResultProducer;
import coddy.repository.DocumentRepository;
import com.google.common.cache.Cache;
import com.google.common.collect.ImmutableSet;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.util.Set;

@Slf4j
@Service
@RequiredArgsConstructor
public class DocumentExecutorService {

    private final DocumentRepository documentRepository;
    private final DocumentContentService documentContentService;
    private final TaskProducer taskProducer;
    private final WsTaskResultProducer wsTaskResultProducer;
    private final Cache<String, TaskDto> taskCache;
    private final Set<TaskResultDto.Status> finalStatuses = ImmutableSet.of(TaskResultDto.Status.ERROR, TaskResultDto.Status.COMPLETED);

    @Transactional(readOnly = true)
    @PreAuthorize("@accessControl.hasPermission(#documentId, 'SHARED')")
    public void submit(String documentId) {
        DocumentView documentView = documentRepository.getById(documentId);

        if(documentView == null) {
            return;
        }

        taskCache.asMap().computeIfAbsent(documentId, id -> {
            sendTaskStartedResult(documentId);
            return submitTask(documentView);
        });
    }

    private TaskDto submitTask(DocumentView documentView) {
        CacheableDocument cacheableDocument = documentContentService.get(documentView.getId());
        TaskDto taskDto = new TaskDto()
                .setContent(cacheableDocument.getValue())
                .setDocumentId(documentView.getId())
                .setMode(documentView.getMode());

        taskProducer.send(taskDto);

        log.debug("Task id - {} has been submitted", taskDto.getDocumentId());

        return taskDto;
    }

    private void sendTaskStartedResult(String id) {
        TaskResultDto taskResultDto = new TaskResultDto().setDocumentId(id).setStatus(TaskResultDto.Status.STARTED);
        wsTaskResultProducer.send(taskResultDto);
    }

    public void handleTaskResult(TaskResultDto taskResultDto) {
        String id = taskResultDto.getDocumentId();
        boolean invalidate = finalStatuses.contains(taskResultDto.getStatus());

        if (taskCache.getIfPresent(id) != null) {
            wsTaskResultProducer.send(taskResultDto);
        }

        if (invalidate) {
            taskCache.invalidate(id);
        }
    }
}