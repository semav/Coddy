package coddy.entity;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.experimental.FieldDefaults;
import org.bitbucket.cowwoc.diffmatchpatch.DiffMatchPatch;
import java.util.LinkedList;
import java.util.concurrent.atomic.AtomicLong;

@Getter
@FieldDefaults(level = AccessLevel.PRIVATE)
public final class CacheableDocument {
    private static final AtomicLong COUNTER = new AtomicLong();

    private final String id;
    private final String value;
    private final long order = COUNTER.incrementAndGet();

    public CacheableDocument(Document document) {
        this.id = document.getId();
        this.value = document.getContent();
    }

    public CacheableDocument(String id, String value) {
        this.id = id;
        this.value = value;
    }

    public CacheableDocument applyPatch(String patch) {
        DiffMatchPatch diff = new DiffMatchPatch();
        LinkedList<DiffMatchPatch.Patch> list = new LinkedList<>(diff.patchFromText(patch));
        String newValue = (String) diff.patchApply(list, value)[0];
        return new CacheableDocument(id, newValue);
    }
}