package coddy.entity;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;
import lombok.experimental.FieldDefaults;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.time.Instant;

@Getter
@Setter
@Accessors(chain = true)
@FieldDefaults(level = AccessLevel.PRIVATE)
@Entity
@Table(name="document")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
public class Document implements DocumentView {
    private static final String DOCUMENT_DEFAULT_MODE = "text";

    @Id
    String id;
    String name;
    String mode;
    Instant lastUpdate = Instant.now();
    Boolean share;
    String content;

    @ManyToOne
    @JoinColumn(name="email")
    private User user;

    public Document() {}

    public Document(String id, String name, User user) {
        this.id = id;
        this.name = name;
        this.user = user;
        this.content = "";
        this.share = true;
        this.mode = DOCUMENT_DEFAULT_MODE;
    }
}