package coddy.entity;

import java.time.Instant;

public interface DocumentView {
    String getId();
    User getUser();
    String getName();
    String getMode();
    Instant getLastUpdate();
    Boolean getShare();
}
