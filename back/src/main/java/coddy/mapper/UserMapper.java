package coddy.mapper;

import coddy.dto.UserDto;
import coddy.entity.User;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMapper {
    UserDto map(User document);

}
