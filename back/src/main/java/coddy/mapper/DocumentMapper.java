package coddy.mapper;

import coddy.dto.DocumentDto;
import coddy.entity.Document;
import coddy.entity.DocumentView;
import org.mapstruct.Mapper;
import java.util.List;

@Mapper(componentModel = "spring")
public interface DocumentMapper {
    DocumentDto map(DocumentView document);
    List<DocumentDto> map(List<DocumentView> document);
    DocumentDto map(Document document);
    Document map(DocumentDto documentDto);
}
