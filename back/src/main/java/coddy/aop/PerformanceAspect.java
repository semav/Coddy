package coddy.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import java.util.concurrent.TimeUnit;

@Aspect
@Component
public class PerformanceAspect {

    private final Logger logger = LoggerFactory.getLogger(PerformanceAspect.class);

    @Pointcut("@within(coddy.aop.PerformanceLoggable) || @annotation(coddy.aop.PerformanceLoggable)")
    public void loggable() {
        // pointcut signature
    }

    @Around("loggable()")
    public Object measureMethodExecutionTime(ProceedingJoinPoint pjp) throws Throwable {
        long start = System.nanoTime();
        Object result = pjp.proceed();
        long end = System.nanoTime();
        String methodName = pjp.getSignature().getName();
        String className = pjp.getSignature().getDeclaringTypeName();
        logger.info("Execution of {}.{}() took {} ms",
                className, methodName, TimeUnit.NANOSECONDS.toMillis(end - start));
        return result;
    }
}
