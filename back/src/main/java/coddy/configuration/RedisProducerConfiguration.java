package coddy.configuration;

import coddy.dto.messsage.TaskDto;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;

@Configuration
public class RedisProducerConfiguration {
    @Bean
    RedisTemplate<String, TaskDto> redisTaskTemplate(RedisConnectionFactory connectionFactory, Jackson2JsonRedisSerializer<TaskDto> serializer) {
        RedisTemplate<String, TaskDto> redisTemplate = new RedisTemplate<>();
        redisTemplate.setConnectionFactory(connectionFactory);
        redisTemplate.setDefaultSerializer(serializer);
        redisTemplate.afterPropertiesSet();
        return redisTemplate;
    }

    @Bean
    public Jackson2JsonRedisSerializer<TaskDto> jackson2JsonRedisSerializer() {
        return new Jackson2JsonRedisSerializer<>(TaskDto.class);
    }
}
