package coddy.configuration;

import coddy.dto.messsage.TaskDto;
import coddy.entity.CacheableDocument;
import coddy.service.CacheableDocumentLoader;
import coddy.service.TaskRemovalListener;
import com.google.common.cache.Cache;
import com.google.common.cache.CacheBuilder;
import com.google.common.cache.LoadingCache;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Configuration
public class CacheConfiguration {
    private static final int DOCUMENT_DURATION_MINUTES = 5;
    private static final int TASK_DURATION_SECONDS = 15;
    private static final int CLEAN_UP_INITIAL_DELAY_SECONDS = 10;
    private static final int CLEAN_UP_DELAY_SECONDS = 2;

    @Bean
    public LoadingCache<String, CacheableDocument> documentCache(CacheableDocumentLoader loader) {
        return CacheBuilder.newBuilder()
                .expireAfterAccess(DOCUMENT_DURATION_MINUTES, TimeUnit.MINUTES)
                .build(loader);
    }

    @Bean
    public Cache<String, TaskDto> taskCache(TaskRemovalListener taskRemovalListener) {
        Cache<String, TaskDto> cache = CacheBuilder.newBuilder()
                .removalListener(taskRemovalListener)
                .expireAfterWrite(TASK_DURATION_SECONDS, TimeUnit.SECONDS)
                .build();

        ScheduledExecutorService executorService = Executors.newSingleThreadScheduledExecutor();
        executorService.scheduleWithFixedDelay(
                cache::cleanUp,
                CLEAN_UP_INITIAL_DELAY_SECONDS,
                CLEAN_UP_DELAY_SECONDS,
                TimeUnit.SECONDS);

        return cache;
    }
}
