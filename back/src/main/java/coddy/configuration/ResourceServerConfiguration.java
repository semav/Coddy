package coddy.configuration;

import coddy.entity.UserPrincipal;
import org.springframework.boot.autoconfigure.security.oauth2.resource.PrincipalExtractor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.web.context.request.RequestContextListener;

@Configuration
@EnableResourceServer
public class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {
    @Override
    public void configure(HttpSecurity http) throws Exception {
        String[] permitApiPatterns = {
                "/documents",
                "/documents/*",
                "/documents/*/content",
                "/documents/*/run",
                "/documents/*/access"
        };

        http.cors().and().csrf().disable()
                .authorizeRequests()
                .antMatchers("/documents/view").authenticated()
                .antMatchers(permitApiPatterns).permitAll()
                .antMatchers("/stomp").permitAll()
                .anyRequest().authenticated();
    }

    @Bean
    PrincipalExtractor principalExtractor() {
        return map -> new UserPrincipal(
                map.get("email").toString().toLowerCase(),
                map.get("name").toString()
        );
    }

    @Bean
    public RequestContextListener requestContextListener() {
        return new RequestContextListener();
    }
}
