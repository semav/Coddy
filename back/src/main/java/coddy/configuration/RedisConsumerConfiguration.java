package coddy.configuration;

import coddy.dto.messsage.TaskResultDto;
import coddy.messaging.TaskResultListener;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.listener.PatternTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;


@Configuration
public class RedisConsumerConfiguration {
    @Value("${executor.taskResult.topic}")
    private String taskResultTopic;

    @Bean
    RedisMessageListenerContainer listenerContainer(RedisConnectionFactory connectionFactory, MessageListenerAdapter listenerAdapter) {
        RedisMessageListenerContainer container = new RedisMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.addMessageListener(listenerAdapter, new PatternTopic(taskResultTopic));

        return container;
    }

    @Bean
    MessageListenerAdapter listenerAdapter(TaskResultListener taskResultListener) {
        MessageListenerAdapter messageListenerAdapter = new MessageListenerAdapter(taskResultListener);
        messageListenerAdapter.setSerializer(new Jackson2JsonRedisSerializer<>(TaskResultDto.class));
        return messageListenerAdapter;
    }

}
