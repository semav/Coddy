package coddy.controller;

import coddy.aop.PerformanceLoggable;
import coddy.dto.UserDto;
import coddy.entity.User;
import coddy.mapper.UserMapper;
import coddy.service.UserService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("users")
@PerformanceLoggable
public class UsersController {

    private final UserService userService;
    private final UserMapper userMapper;

    public UsersController(UserService userService, UserMapper userMapper) {
        this.userService = userService;
        this.userMapper = userMapper;
    }

    @PostMapping("/signin")
    public UserDto signIn() {
        User user = userService.signIn();
        return userMapper.map(user);
    }
}