package coddy.controller;

import coddy.aop.PerformanceLoggable;
import coddy.dto.DocumentDto;
import coddy.entity.Document;
import coddy.entity.DocumentView;
import coddy.mapper.DocumentMapper;
import coddy.service.AccessControl;
import coddy.service.DocumentAccess;
import coddy.service.DocumentContentService;
import coddy.service.DocumentExecutorService;
import coddy.service.DocumentService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;
import java.util.List;

@RestController
@RequestMapping("documents")
@PerformanceLoggable
public class DocumentsController {
    private final DocumentMapper documentMapper;
    private final DocumentService documentService;
    private final DocumentContentService documentContentService;
    private final DocumentExecutorService documentExecutorService;

    private final AccessControl accessControl;

    public DocumentsController(
            DocumentMapper documentMapper,
            DocumentService documentService,
            DocumentContentService documentContentService,
            DocumentExecutorService documentExecutorService,
            AccessControl accessControl
    ) {
        this.documentMapper = documentMapper;
        this.documentService = documentService;
        this.documentContentService = documentContentService;
        this.documentExecutorService = documentExecutorService;
        this.accessControl = accessControl;
    }

    @GetMapping("/{id}")
    public DocumentDto get(@PathVariable String id) {
        DocumentView documentView = documentService.get(id);
        if (documentView == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return documentMapper.map(documentView);
    }

    @GetMapping(value = "/{id}/access")
    public String getAccess(@PathVariable String id) {
        DocumentAccess access = accessControl.getAccess(id.toLowerCase());
        return access == null ? null : access.name();
    }

    @GetMapping
    public List<DocumentDto> getList() {
        List<DocumentView> documents = documentService.getList();
        return documentMapper.map(documents);
    }

    @GetMapping(value = "/{id}/content")
    public String getContent(@PathVariable String id) {
        return documentContentService.get(id.toLowerCase()).getValue();
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(value = HttpStatus.NO_CONTENT)
    public void delete(@PathVariable String id) {
        documentService.delete(id.toLowerCase());
    }

    @PostMapping("/{id}/run")
    @ResponseStatus(value = HttpStatus.OK)
    public void run(@PathVariable String id) {
        documentExecutorService.submit(id.toLowerCase());
    }

    @PostMapping(headers = "Accept=application/json")
    public DocumentDto create() {
        DocumentView documentView = documentService.create();
        return documentMapper.map(documentView);
    }

    @PatchMapping(headers = "Accept=application/json")
    public DocumentDto update(@RequestBody DocumentDto documentDto) {
        Document document = documentMapper.map(documentDto);
        document = documentService.update(document.getId(), document);
        return documentMapper.map(document);
    }
}