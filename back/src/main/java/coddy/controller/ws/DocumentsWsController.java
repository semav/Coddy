package coddy.controller.ws;

import coddy.service.DocumentContentService;
import org.springframework.messaging.handler.annotation.DestinationVariable;
import org.springframework.messaging.handler.annotation.Header;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Controller;

@Controller
public class DocumentsWsController {
    private final DocumentContentService documentContentService;

    public DocumentsWsController(DocumentContentService documentContentService) {
        this.documentContentService = documentContentService;
    }

    @MessageMapping("/documents/{id}/patch")
    public void patches(@DestinationVariable String id, @Payload String patch, @Header String patchId) {
        documentContentService.applyPatches(id, patch, patchId);
    }
}