package coddy.messaging;

import coddy.dto.messsage.TaskResultDto;
import coddy.service.DocumentExecutorService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class TaskResultListener {

    private final DocumentExecutorService documentExecutorService;

    public void handleMessage(TaskResultDto taskResultDto) {
        log.debug("Handle task result. {}", taskResultDto);

        documentExecutorService.handleTaskResult(taskResultDto);
    }
}
