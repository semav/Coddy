package coddy.messaging;

import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;
import java.util.HashMap;
import java.util.Map;

@Service
public class WsPatchProducer {
    private final SimpMessagingTemplate simpMessagingTemplate;

    public WsPatchProducer(SimpMessagingTemplate simpMessagingTemplate) {
        this.simpMessagingTemplate = simpMessagingTemplate;
    }

    public void send(String id, String patch, String patchId) {
        String destination = String.format("/topic/documents/%s/patch", id);
        Map<String, Object> headers = new HashMap<>();
        headers.put("patchId", patchId);

        simpMessagingTemplate.convertAndSend(destination, patch, headers);
    }
}
