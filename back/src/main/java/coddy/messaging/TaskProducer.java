package coddy.messaging;

import coddy.dto.messsage.TaskDto;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

@Service
public class TaskProducer {
    @Value("${executor.task.topic}")
    private String taskTopic;

    private final RedisTemplate<String, TaskDto> redisTaskTemplate;

    TaskProducer(RedisTemplate<String, TaskDto> redisTaskTemplate) {
        this.redisTaskTemplate = redisTaskTemplate;
    }

    public void send(TaskDto taskDto) {
        redisTaskTemplate.convertAndSend(taskTopic, taskDto);
    }
}
