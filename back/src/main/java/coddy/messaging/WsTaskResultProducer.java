package coddy.messaging;

import coddy.dto.messsage.TaskResultDto;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

@Service
public class WsTaskResultProducer {
    private final SimpMessagingTemplate simpMessagingTemplate;

    public WsTaskResultProducer(SimpMessagingTemplate simpMessagingTemplate) {
        this.simpMessagingTemplate = simpMessagingTemplate;
    }

    public void send(TaskResultDto taskResultDto) {
        final String destination = String.format("/topic/documents/%s/executor", taskResultDto.getDocumentId());

        simpMessagingTemplate.convertAndSend(destination, taskResultDto);
    }
}
