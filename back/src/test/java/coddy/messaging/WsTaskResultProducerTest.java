package coddy.messaging;

import static org.mockito.Mockito.verify;

import coddy.dto.messsage.TaskResultDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.messaging.simp.SimpMessagingTemplate;

@ExtendWith(MockitoExtension.class)
class WsTaskResultProducerTest {

    @InjectMocks
    WsTaskResultProducer wsTaskResultProducer;

    @Mock
    SimpMessagingTemplate simpMessagingTemplate;

    @Test
    void shouldSend() {
        TaskResultDto taskResultDto = new TaskResultDto();
        taskResultDto.setDocumentId("id");

        wsTaskResultProducer.send(taskResultDto);

        verify(simpMessagingTemplate).convertAndSend("/topic/documents/id/executor", taskResultDto);
    }
}