package coddy.messaging;

import static org.mockito.Mockito.verify;

import coddy.dto.messsage.TaskDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.util.ReflectionTestUtils;

@ExtendWith(MockitoExtension.class)
class TaskProducerTest {

    @InjectMocks
    TaskProducer taskProducer;

    @Mock
    RedisTemplate<String, TaskDto> redisTemplate;

    @Test
    void shouldSend() {
        TaskDto taskDto = new TaskDto();
        ReflectionTestUtils.setField(taskProducer, "taskTopic", "topic");

        taskProducer.send(taskDto);

        verify(redisTemplate).convertAndSend("topic", taskDto);
    }
}