package coddy.messaging;

import static org.mockito.Mockito.verify;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import java.util.HashMap;
import java.util.Map;

@ExtendWith(MockitoExtension.class)
class WsPatchProducerTest {

    @InjectMocks
    WsPatchProducer wsPatchProducer;

    @Mock
    SimpMessagingTemplate simpMessagingTemplate;

    @Test
    void shouldSend() {
        String id = "id";
        String patch = "patch";
        String patchId = "patchId";
        Map<String, Object> headers = new HashMap<>();

        headers.put("patchId", patchId);

        wsPatchProducer.send(id, patch, patchId);

        verify(simpMessagingTemplate).convertAndSend("/topic/documents/id/patch", patch, headers);
    }
}