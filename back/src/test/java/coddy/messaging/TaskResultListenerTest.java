package coddy.messaging;

import static org.mockito.Mockito.verify;

import coddy.dto.messsage.TaskResultDto;
import coddy.service.DocumentExecutorService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class TaskResultListenerTest {

    @InjectMocks
    TaskResultListener taskResultListener;

    @Mock
    DocumentExecutorService documentExecutorService;

    @Test
    void shouldPassResultToDocumentExecutorService() {
        TaskResultDto taskResultDto = new TaskResultDto();

        taskResultListener.handleMessage(taskResultDto);

        verify(documentExecutorService).handleTaskResult(taskResultDto);
    }
}