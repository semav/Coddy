package coddy.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;
import static org.mockito.Mockito.when;

import coddy.entity.Document;
import coddy.entity.DocumentView;
import coddy.repository.DocumentRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class DocumentServiceTests {

    @InjectMocks
    DocumentService documentService;

    @Mock
    DocumentRepository documentRepository;

    @Test
    void shouldReturnDocument() {
        String id = "id";
        Document document = new Document();

        when(documentRepository.getById(id)).thenReturn(document);

        DocumentView actual = documentService.get(id);

        assertEquals(document, actual);
    }

    @Test
    void shouldReturnNullWhenNotFound() {
        when(documentRepository.getById("id")).thenReturn(null);

        DocumentView actual = documentService.get("id");

        assertNull(actual);
    }
}
