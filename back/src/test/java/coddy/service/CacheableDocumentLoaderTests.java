package coddy.service;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.Mockito.when;

import coddy.entity.CacheableDocument;
import coddy.entity.Document;
import coddy.repository.DocumentRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
class CacheableDocumentLoaderTests {

    @InjectMocks
    CacheableDocumentLoader cacheableDocumentLoader;

    @Mock
    DocumentRepository documentRepository;

    @Test
    void shouldLoad() {
        String id = "id";
        Document document = new Document();

        when(documentRepository.findById(id)).thenReturn(Optional.of(document));

        CacheableDocument actual = cacheableDocumentLoader.load(id);

        assertNotNull(actual);
    }
}
