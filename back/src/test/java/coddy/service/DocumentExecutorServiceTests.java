package coddy.service;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.mockito.Mockito.when;

import coddy.dto.messsage.TaskDto;
import coddy.dto.messsage.TaskResultDto;
import coddy.entity.CacheableDocument;
import coddy.entity.Document;
import coddy.messaging.TaskProducer;
import coddy.messaging.WsTaskResultProducer;
import coddy.repository.DocumentRepository;
import com.google.common.cache.Cache;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.EnumSource;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.concurrent.ConcurrentHashMap;

@ExtendWith(MockitoExtension.class)
class DocumentExecutorServiceTests {

    private final String ID = "id";
    @InjectMocks
    DocumentExecutorService documentExecutorService;
    @Mock
    DocumentRepository documentRepository;
    @Mock
    DocumentContentService documentContentService;
    @Mock
    TaskProducer taskProducer;
    @Mock
    Cache<String, TaskDto> taskCache;
    @Mock
    WsTaskResultProducer wsTaskResultProducer;

    @Test
    void shouldNotSendTaskWhenDocumentDoesntExist() {
        documentExecutorService.submit(ID);

        verifyNoInteractions(taskProducer);
        verifyNoInteractions(taskCache);
    }

    @Test
    void shouldNotSendTaskWhenTaskInProgress() {
        Document document = new Document();
        ConcurrentHashMap<String, TaskDto> map = new ConcurrentHashMap<>();

        map.put(ID, new TaskDto());
        when(documentRepository.getById(ID)).thenReturn(document);
        when(taskCache.asMap()).thenReturn(map);

        documentExecutorService.submit(ID);

        verifyNoInteractions(taskProducer);
    }

    @Test
    void shouldSendTaskToExecutorAndPutIntoCache() {
        Document document = new Document().setId(ID).setContent("content").setMode("mode");
        CacheableDocument cacheableDocument = new CacheableDocument(document);
        TaskDto taskDto = new TaskDto().setDocumentId(ID).setContent("content").setMode("mode");
        ConcurrentHashMap<String, TaskDto> map = new ConcurrentHashMap<>();

        when(documentRepository.getById(ID)).thenReturn(document);
        when(documentContentService.get(ID)).thenReturn(cacheableDocument);
        when(taskCache.asMap()).thenReturn(map);

        documentExecutorService.submit(ID);

        verify(taskProducer).send(taskDto);
        assertEquals(taskDto, map.get(ID));
    }

    @Test
    void shouldSendTaskResultToWsWhenNewTaskIsSubmitted() {
        Document document = new Document().setId(ID);
        CacheableDocument cacheableDocument = new CacheableDocument(document);
        TaskResultDto taskResultDto = new TaskResultDto().setDocumentId(ID).setStatus(TaskResultDto.Status.STARTED);
        ConcurrentHashMap<String, TaskDto> map = new ConcurrentHashMap<>();

        when(documentRepository.getById(ID)).thenReturn(document);
        when(documentContentService.get(ID)).thenReturn(cacheableDocument);
        when(taskCache.asMap()).thenReturn(map);

        documentExecutorService.submit(ID);

        verify(wsTaskResultProducer).send(taskResultDto);
    }

    @Test
    void shouldIgnoreTaskResultWhenTaskIsNotInCache() {
        TaskResultDto taskResultDto = new TaskResultDto().setDocumentId(ID).setStatus(TaskResultDto.Status.IN_PROGRESS);

        documentExecutorService.handleTaskResult(taskResultDto);

        verifyNoInteractions(wsTaskResultProducer);
    }

    @Test
    void shouldSendTaskResultToWsWhenTaskIsInCache() {
        TaskResultDto taskResultDto = new TaskResultDto().setDocumentId(ID).setStatus(TaskResultDto.Status.IN_PROGRESS);

        when(taskCache.getIfPresent(ID)).thenReturn(new TaskDto());

        documentExecutorService.handleTaskResult(taskResultDto);

        verify(wsTaskResultProducer).send(taskResultDto);
    }

    @ParameterizedTest
    @EnumSource(value = TaskResultDto.Status.class, names = {"COMPLETED", "ERROR"})
    void shouldRemoveTaskFromCacheWhenFinalStatus(TaskResultDto.Status status) {
        TaskResultDto taskResultDto = new TaskResultDto().setDocumentId(ID).setStatus(status);

        when(taskCache.getIfPresent(ID)).thenReturn(new TaskDto());

        documentExecutorService.handleTaskResult(taskResultDto);

        verify(wsTaskResultProducer).send(taskResultDto);
        verify(taskCache).invalidate(ID);
    }
}