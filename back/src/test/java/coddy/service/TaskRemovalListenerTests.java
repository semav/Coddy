package coddy.service;

import static org.mockito.Mockito.verify;

import coddy.dto.messsage.TaskDto;
import coddy.dto.messsage.TaskResultDto;
import coddy.messaging.WsTaskResultProducer;
import com.google.common.cache.RemovalCause;
import com.google.common.cache.RemovalNotification;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class TaskRemovalListenerTests {

    @InjectMocks
    TaskRemovalListener taskRemovalListener;

    @Mock
    WsTaskResultProducer wsTaskResultProducer;

    @Test
    void shouldSendTaskResultToWs() {
        String id = "id";
        TaskResultDto taskResultDto = new TaskResultDto()
                .setDocumentId(id)
                .setMessage("Execution took too long")
                .setStatus(TaskResultDto.Status.ERROR);

        taskRemovalListener.onRemoval(RemovalNotification.create(id, new TaskDto(), RemovalCause.EXPIRED));

        verify(wsTaskResultProducer).send(taskResultDto);
    }
}