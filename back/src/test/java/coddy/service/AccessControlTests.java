package coddy.service;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

import coddy.entity.Document;
import coddy.entity.User;
import coddy.entity.UserPrincipal;
import coddy.repository.DocumentRepository;
import coddy.service.AccessControl;
import coddy.service.AuthenticationService;
import coddy.service.DocumentAccess;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class AccessControlTests {

    @InjectMocks
    AccessControl accessControl;

    @Mock
    DocumentRepository documentRepository;

    @Mock
    AuthenticationService authenticationService;

    @Test
    void shouldReturnTrueWhenUserHasFullAccess() {
        String id = "id";
        UserPrincipal userPrincipal = new UserPrincipal("email", null);
        Document document = new Document(id, null, new User().setEmail("email")).setShare(Boolean.FALSE);

        when(documentRepository.getById(id)).thenReturn(document);
        when(authenticationService.getUserPrincipal()).thenReturn(userPrincipal);

        boolean actual = accessControl.hasPermission(id, DocumentAccess.FULL);

        assertTrue(actual);
    }

    @Test
    void shouldReturnTrueWhenUserHasSharedAccess() {
        String id = "id";
        UserPrincipal userPrincipal = new UserPrincipal("otherEmail", null);
        Document document = new Document(id, null, new User().setEmail("email")).setShare(Boolean.TRUE);

        when(documentRepository.getById(id)).thenReturn(document);
        when(authenticationService.getUserPrincipal()).thenReturn(userPrincipal);

        boolean actual = accessControl.hasPermission(id, DocumentAccess.SHARED);

        assertTrue(actual);
    }

    @Test
    void shouldReturnFalseWhenUserHasNoAccess() {
        String id = "id";
        UserPrincipal userPrincipal = new UserPrincipal("otherEmail", null);
        Document document = new Document(id, null, new User().setEmail("email")).setShare(Boolean.FALSE);

        when(documentRepository.getById(id)).thenReturn(document);
        when(authenticationService.getUserPrincipal()).thenReturn(userPrincipal);

        boolean actual = accessControl.hasPermission(id, DocumentAccess.SHARED);

        assertFalse(actual);
    }

    @Test
    void shouldReturnTrueWhenSharedAndUnauthenticated() {
        String id = "id";
        Document document = new Document(id, null, new User().setEmail("email")).setShare(Boolean.TRUE);

        when(documentRepository.getById(id)).thenReturn(document);
        when(authenticationService.getUserPrincipal()).thenReturn(null);

        boolean actual = accessControl.hasPermission(id, DocumentAccess.SHARED);

        assertTrue(actual);
    }

    @Test
    void shouldReturnTrueWhenDocumentDoesntExist() {
        String id = "id";

        when(documentRepository.getById(id)).thenReturn(null);

        boolean actual = accessControl.hasPermission(id, DocumentAccess.SHARED);

        assertTrue(actual);
    }

    @Test
    void shouldReturnTrueWhenDocumentUserIsNull() {
        String id = "id";
        Document document = new Document(id, null, null).setShare(Boolean.TRUE);

        when(documentRepository.getById(id)).thenReturn(document);

        boolean actual = accessControl.hasPermission(id, DocumentAccess.SHARED);

        assertTrue(actual);
    }


}
