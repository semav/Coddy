package coddy.service;

import static org.mockito.Mockito.timeout;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import coddy.entity.CacheableDocument;
import coddy.entity.Document;
import coddy.repository.DocumentRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.time.Instant;

@ExtendWith(MockitoExtension.class)
class DocumentPersistServiceTests {

    @InjectMocks
    DocumentPersistService documentPersistService;

    @Mock
    DocumentRepository documentRepository;

    @Test
    void shouldPersistLastValue() throws InterruptedException {
        String id = "id";
        Document document = new Document();

        when(documentRepository.findById(id)).thenReturn(java.util.Optional.of(document));

        documentPersistService.persist(new CacheableDocument(id, "value"), Instant.now());

        documentPersistService.runPersistThread();
        verify(documentRepository, timeout(1000)).save(document);
        documentPersistService.stopPersistThread();
    }
}
