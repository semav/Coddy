package coddy.controller;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import coddy.dto.DocumentDto;
import coddy.entity.CacheableDocument;
import coddy.entity.Document;
import coddy.entity.DocumentView;
import coddy.mapper.DocumentMapper;
import coddy.service.AccessControl;
import coddy.service.DocumentAccess;
import coddy.service.DocumentContentService;
import coddy.service.DocumentExecutorService;
import coddy.service.DocumentService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.Collections;
import java.util.List;

@ExtendWith(MockitoExtension.class)
class DocumentControllerTests {

    @InjectMocks
    DocumentsController documentsController;

    @Mock
    AccessControl accessControl;

    @Mock
    DocumentMapper documentMapper;

    @Mock
    DocumentService documentService;

    @Mock
    DocumentContentService documentContentService;

    @Mock
    DocumentExecutorService documentExecutorService;

    private static final String ID = "id";

    @Test
    void shouldReturnDocument() {
        Document document = new Document().setId(ID);
        DocumentDto expected = new DocumentDto();

        when(documentService.get(ID)).thenReturn(document);
        when(documentMapper.map((DocumentView) document)).thenReturn(expected);

        DocumentDto actual = documentsController.get(ID);

        assertEquals(expected, actual);
    }

    @Test
    void shouldReturnAccess() {
        when(accessControl.getAccess(ID)).thenReturn(DocumentAccess.FULL);

        String actual = documentsController.getAccess(ID);

        assertEquals(DocumentAccess.FULL.name(), actual);
    }

    @Test
    void shouldReturnList() {
        List<DocumentView> documents = Collections.emptyList();
        List<DocumentDto> expected = Collections.emptyList();

        when(documentService.getList()).thenReturn(documents);
        when(documentMapper.map(documents)).thenReturn(expected);

        List<DocumentDto> actual = documentsController.getList();

        assertEquals(expected, actual);
    }

    @Test
    void shouldReturnContent() {
        String expected = "content";
        CacheableDocument document = new CacheableDocument(ID, expected);
        when(documentContentService.get(ID)).thenReturn(document);

        String actual = documentsController.getContent(ID);

        assertEquals(expected, actual);
    }

    @Test
    void shouldDelete() {
        documentsController.delete(ID);

        verify(documentService).delete(ID);
    }

    @Test
    void shouldRun() {
        documentsController.run(ID);

        verify(documentExecutorService).submit(ID);
    }

    @Test
    void shouldCreate() {
        DocumentView document = new Document();
        DocumentDto expected = new DocumentDto();

        when(documentService.create()).thenReturn(document);
        when(documentMapper.map(document)).thenReturn(expected);

        DocumentDto actual = documentsController.create();

        assertEquals(expected, actual);
    }

    @Test
    void shouldUpdate() {
        DocumentDto documentDto = new DocumentDto();
        Document document = new Document().setId(ID);
        Document updated = new Document().setId(ID);
        DocumentDto expected = new DocumentDto();

        when(documentMapper.map(documentDto)).thenReturn(document);
        when(documentService.update(ID, document)).thenReturn(updated);
        when(documentMapper.map(updated)).thenReturn(expected);

        DocumentDto actual = documentsController.update(documentDto);

        assertEquals(expected, actual);
    }
}
