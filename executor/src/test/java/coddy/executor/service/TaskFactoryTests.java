package coddy.executor.service;


import static org.junit.jupiter.api.Assertions.assertTrue;

import coddy.executor.dto.TaskDto;
import coddy.executor.task.JavaTask;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
class TaskFactoryTests {

    @InjectMocks
    TaskFactory taskFactory;

    @Test
    void shouldReturnJavaTask() {
        TaskDto taskDto = new TaskDto();
        taskDto.setMode("java");

        Optional<Runnable> task = taskFactory.getTask(taskDto);

        assertTrue(task.isPresent());
        assertTrue(task.get() instanceof JavaTask);
    }
}