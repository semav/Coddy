package coddy.executor.service;


import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import coddy.executor.dto.TaskDto;
import coddy.executor.messaging.TaskResultProducer;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.util.Optional;

@ExtendWith(MockitoExtension.class)
class TaskExecutorServiceTests {

    @Mock
    TaskFactory taskFactory;

    @Mock
    TaskResultProducer taskResultProducer;

    @Mock
    Runnable runnable;

    private TaskExecutorService taskExecutorService;

    @BeforeEach
    void beforeEach() {
        taskExecutorService = new TaskExecutorService(taskFactory, taskResultProducer,1);
    }

    @Test
    void shouldSubmitTask() throws InterruptedException {
        TaskDto taskDto = new TaskDto();
        when(taskFactory.getTask(taskDto)).thenReturn(Optional.of(runnable));

        taskExecutorService.submit(taskDto);
        taskExecutorService.stopTaskExecutorService();

        verify(runnable).run();
    }
}