package coddy.executor.task;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import coddy.executor.dto.TaskDto;
import coddy.executor.messaging.TaskResultProducer;
import coddy.executor.service.FileService;
import coddy.executor.service.RuntimeService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;

@ExtendWith(MockitoExtension.class)
class JavaTaskTests {

    @InjectMocks
    JavaTask javaTask;

    @Mock
    TaskResultProducer taskResultService;

    @Mock
    RuntimeService runtimeService;

    @Mock
    FileService fileService;

    @Mock
    TaskDto taskDto;

    @Test
    void shouldMakeAndDeleteFile() throws IOException, InterruptedException {
        Path path = Paths.get("path");

        when(taskDto.getContent()).thenReturn("content");
        when(fileService.make("Main.java", "content")).thenReturn(path);

        javaTask.run();

        verify(fileService).delete(path);
    }

    @Test
    void shouldCompileAndRun() throws IOException, InterruptedException {
        Path path = Paths.get("path");

        when(taskDto.getContent()).thenReturn("content");
        when(fileService.make("Main.java", "content")).thenReturn(path);

        javaTask.run();

        verify(runtimeService).execCommand(path, "javac Main.java");
        verify(runtimeService).execCommand(path, "java -Xmx50m -XX:+ExitOnOutOfMemoryError Main");
    }

    // todo more tests
}
