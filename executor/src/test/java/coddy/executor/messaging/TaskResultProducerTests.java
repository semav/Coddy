package coddy.executor.messaging;

import static org.mockito.Mockito.verify;

import coddy.executor.dto.TaskResultDto;
import coddy.executor.messaging.TaskResultProducer;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.util.ReflectionTestUtils;

@ExtendWith(MockitoExtension.class)
class TaskResultProducerTests {

    @InjectMocks
    TaskResultProducer taskResultProducer;

    @Mock
    RedisTemplate<String, TaskResultDto> redisTaskResultTemplate;

    @Test
    void shouldSend() {
        TaskResultDto taskResultDto = new TaskResultDto();
        ReflectionTestUtils.setField(taskResultProducer, "taskResultTopic", "topic");

        taskResultProducer.send(taskResultDto);

        verify(redisTaskResultTemplate).convertAndSend("topic", taskResultDto);
    }
}