package coddy.executor.messaging;


import static org.mockito.Mockito.verify;

import coddy.executor.dto.TaskDto;
import coddy.executor.service.TaskExecutorService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class TaskListenerTests {

    @InjectMocks
    TaskListener taskListener;

    @Mock
    TaskExecutorService taskExecutorService;

    @Test
    void shouldSubmitTask() {
        TaskDto taskDto = new TaskDto();

        taskListener.handleMessage(taskDto);

        verify(taskExecutorService).submit(taskDto);
    }
}