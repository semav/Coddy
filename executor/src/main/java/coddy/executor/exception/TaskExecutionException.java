package coddy.executor.exception;

public class TaskExecutionException extends RuntimeException {
    public TaskExecutionException(String message) {
        super(message);
    }
}
