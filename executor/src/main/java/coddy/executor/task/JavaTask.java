package coddy.executor.task;

import coddy.executor.dto.TaskDto;
import coddy.executor.service.FileService;
import coddy.executor.service.RuntimeService;
import coddy.executor.messaging.TaskResultProducer;

public class JavaTask extends AbstractTack {
    public JavaTask(TaskResultProducer taskResultService,
                    RuntimeService runtimeService,
                    FileService fileService,
                    TaskDto taskDto) {
        super(taskResultService, runtimeService, fileService, taskDto);
    }

    public static final String MODE = "java";

    @Override
    protected boolean isCompiled() {
        return true;
    }

    @Override
    protected String getCompileCommand() {
        return "javac Main.java";
    }

    @Override
    protected String getRunCommand() {
        return "java -Xmx50m -XX:+ExitOnOutOfMemoryError Main";
    }

    @Override
    protected String getFileName() {
        return "Main.java";
    }
}
