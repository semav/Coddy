package coddy.executor.task;

import coddy.executor.dto.TaskDto;
import coddy.executor.messaging.TaskResultProducer;
import coddy.executor.service.FileService;
import coddy.executor.service.RuntimeService;

public class TypeScriptTask extends AbstractTack {
    public TypeScriptTask(TaskResultProducer taskResultService,
                          RuntimeService runtimeService,
                          FileService fileService,
                          TaskDto taskDto) {
        super(taskResultService, runtimeService, fileService, taskDto);
    }

    public static final String MODE = "typescript";

    @Override
    protected boolean isCompiled() {
        return true;
    }

    @Override
    protected String getCompileCommand() {
        return "tsc -t ES2020 Main.ts";
    }

    @Override
    protected String getRunCommand() {
        return "node --max-old-space-size=30 Main.js";
    }

    @Override
    protected String getFileName() {
        return "Main.ts";
    }
}
