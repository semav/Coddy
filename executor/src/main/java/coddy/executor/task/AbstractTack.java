package coddy.executor.task;

import coddy.executor.dto.TaskDto;
import coddy.executor.dto.TaskResultDto;
import coddy.executor.exception.TaskExecutionException;
import coddy.executor.messaging.TaskResultProducer;
import coddy.executor.service.FileService;
import coddy.executor.service.RuntimeService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import java.io.IOException;
import java.nio.file.Path;

@Slf4j
@RequiredArgsConstructor
public abstract class AbstractTack implements Runnable {

    private final TaskResultProducer taskResultService;
    private final RuntimeService runtimeService;
    private final FileService fileService;
    private final TaskDto taskDto;

    @Override
    public void run() {
        Path path = null;

        log.debug("Started id - {}", taskDto.getDocumentId());

        try {
            path = fileService.make(getFileName(), taskDto.getContent());
            compile(path);
            run(path);
        }
        catch (TaskExecutionException e) {
            sendErrorResult(e.getMessage());
        }
        catch (Exception e) {
            sendErrorResult("Error! Something went wrong.");
            log.error("Task execution error", e);
        }
        finally {
            fileService.delete(path);
        }
        log.debug("Completed id - {}", taskDto.getDocumentId());
    }

    private void run(Path path) throws IOException, InterruptedException {
        String message = runtimeService.execCommand(path, getRunCommand());
        taskResultService.send(new TaskResultDto(getId(), message, TaskResultDto.Status.COMPLETED));
    }

    private void compile(Path path) throws IOException, InterruptedException {
        if (isCompiled()) {
            taskResultService.send(new TaskResultDto(getId(), "Compiling...", TaskResultDto.Status.IN_PROGRESS));
            String message = runtimeService.execCommand(path, getCompileCommand());
            taskResultService.send(new TaskResultDto(getId(), message, TaskResultDto.Status.IN_PROGRESS));
        }
    }

    private void sendErrorResult(String message) {
        TaskResultDto resultDto = new TaskResultDto(getId(), message, TaskResultDto.Status.ERROR);
        taskResultService.send(resultDto);
    }

    private String getId() {
        return taskDto.getDocumentId();
    }

    protected abstract boolean isCompiled();

    protected abstract String getCompileCommand();

    protected abstract String getRunCommand();

    protected abstract String getFileName();

}