package coddy.executor.task;

import coddy.executor.dto.TaskDto;
import coddy.executor.messaging.TaskResultProducer;
import coddy.executor.service.FileService;
import coddy.executor.service.RuntimeService;

public class PythonTask extends AbstractTack {
    public PythonTask(TaskResultProducer taskResultService,
                      RuntimeService runtimeService,
                      FileService fileService,
                      TaskDto taskDto) {
        super(taskResultService, runtimeService, fileService, taskDto);
    }

    public static final String MODE = "python";

    @Override
    protected boolean isCompiled() {
        return false;
    }

    @Override
    protected String getCompileCommand() {
        return null;
    }

    @Override
    protected String getRunCommand() {
        return "python3 Main.py";
    }

    @Override
    protected String getFileName() {
        return "Main.py";
    }
}
