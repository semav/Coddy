package coddy.executor.task;

import coddy.executor.dto.TaskDto;
import coddy.executor.messaging.TaskResultProducer;
import coddy.executor.service.FileService;
import coddy.executor.service.RuntimeService;

public class JavaScriptTask extends AbstractTack {
    public JavaScriptTask(TaskResultProducer taskResultService,
                          RuntimeService runtimeService,
                          FileService fileService,
                          TaskDto taskDto) {
        super(taskResultService, runtimeService, fileService, taskDto);
    }

    public static final String MODE = "javascript";

    @Override
    protected boolean isCompiled() {
        return false;
    }

    @Override
    protected String getCompileCommand() {
        return null;
    }

    @Override
    protected String getRunCommand() {
        return "node --max-old-space-size=30 Main.js";
    }

    @Override
    protected String getFileName() {
        return "Main.js";
    }
}
