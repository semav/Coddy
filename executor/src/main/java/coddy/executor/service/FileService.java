package coddy.executor.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.UUID;

@Slf4j
@Service
public class FileService {
    public Path make(String fileName, String content) throws IOException {
        Path directoryPath = Paths.get("tmp", UUID.randomUUID().toString());
        Path filePath = Paths.get(directoryPath.toString(), fileName);
        log.debug("Creating directory {}", directoryPath.toString());
        Files.createDirectories(directoryPath);

        log.debug("Creating file {}", filePath.toString());
        try (FileOutputStream fileOutputStream = new FileOutputStream(filePath.toString())) {
            log.debug("Writing file {}", filePath.toString());
            fileOutputStream.write(content.getBytes());
        }

        return directoryPath;
    }

    public void delete(Path path) {
        try {
            FileSystemUtils.deleteRecursively(path);
        } catch (IOException e) {
            log.error("Delete path error", e);
        }
    }

}
