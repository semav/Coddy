package coddy.executor.service;

import coddy.executor.exception.TaskExecutionException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Path;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Slf4j
@Service
public class RuntimeService {

    private static final long TIMEOUT = 8L;
    private static final int LINE_MAX_LENGTH = 500;
    private static final int LINES_LIMIT = 100;

    public String execCommand(Path path, String command) throws IOException, InterruptedException {
        Runtime runtime = Runtime.getRuntime();
        Process proc = runtime.exec(command, null, new File(path.toString()));

        if (!proc.waitFor(TIMEOUT, TimeUnit.SECONDS)) {
            proc.destroyForcibly();
            throw new TaskExecutionException("Execution took too long." );
        }

        String output = getOutput(proc);

        if (proc.exitValue() != 0) {
            throw new TaskExecutionException(output);
        }

        return output;
    }

    private String getOutput(Process proc) throws IOException {
        return Stream.of(
                getOutput(proc.getErrorStream()),
                getOutput(proc.getInputStream()),
                "Process finished with exit code " + proc.exitValue())
            .filter(s -> !StringUtils.isEmpty(s))
            .collect(Collectors.joining("\n"));
    }

    private String getOutput(InputStream inputStream) throws IOException {
        try(InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
            BufferedReader bufferedReader = new BufferedReader(inputStreamReader)) {

            return bufferedReader.lines()
                    .limit(LINES_LIMIT)
                    .map(RuntimeService::cutLine)
                    .collect(Collectors.joining("\n"));
        }
    }

    private static String cutLine(String line) {
        return line.length() <= LINE_MAX_LENGTH ? line : line.substring(0, LINE_MAX_LENGTH);
    }

}
