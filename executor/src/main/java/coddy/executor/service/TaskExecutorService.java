package coddy.executor.service;

import coddy.executor.dto.TaskDto;
import coddy.executor.dto.TaskResultDto;
import coddy.executor.messaging.TaskResultProducer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import javax.annotation.PreDestroy;
import java.util.Optional;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@Service
public class TaskExecutorService {

    private final int SHUTDOWN_TIMEOUT = 5;

    private final ExecutorService executorService;
    private final TaskFactory taskFactory;
    private final TaskResultProducer taskResultService;

    public TaskExecutorService(TaskFactory taskFactory, TaskResultProducer taskResultService, @Value("${executor.thread.number}") int threadNumber) {
        this.executorService = Executors.newFixedThreadPool(threadNumber);
        this.taskFactory = taskFactory;
        this.taskResultService = taskResultService;
    }

    public void submit(TaskDto taskDto) {
        Optional<Runnable> task = taskFactory.getTask(taskDto);
        if (task.isPresent()) {
            executorService.submit(task.get());
        }
        else {
            TaskResultDto resultDto = new TaskResultDto(taskDto.getDocumentId(), "Unsupported mode.", TaskResultDto.Status.ERROR);
            taskResultService.send(resultDto);
        }
    }

    @PreDestroy
    public void stopTaskExecutorService() throws InterruptedException {
        executorService.shutdown();
        executorService.awaitTermination(SHUTDOWN_TIMEOUT, TimeUnit.SECONDS);
    }
}
