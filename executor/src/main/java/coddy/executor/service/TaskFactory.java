package coddy.executor.service;

import coddy.executor.dto.TaskDto;
import coddy.executor.messaging.TaskResultProducer;
import coddy.executor.task.JavaScriptTask;
import coddy.executor.task.JavaTask;
import coddy.executor.task.PythonTask;
import coddy.executor.task.TypeScriptTask;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import java.util.Optional;

@Slf4j
@Service
@RequiredArgsConstructor
public class TaskFactory {
    private final TaskResultProducer taskResultService;
    private final RuntimeService runtimeService;
    private final FileService fileService;

    public Optional<Runnable> getTask(TaskDto taskDto) {
        if (JavaTask.MODE.equalsIgnoreCase(taskDto.getMode())) {
            return Optional.of(new JavaTask(taskResultService, runtimeService, fileService, taskDto));
        }

        if (JavaScriptTask.MODE.equalsIgnoreCase(taskDto.getMode())) {
            return Optional.of(new JavaScriptTask(taskResultService, runtimeService, fileService, taskDto));
        }

        if (PythonTask.MODE.equalsIgnoreCase(taskDto.getMode())) {
            return Optional.of(new PythonTask(taskResultService, runtimeService, fileService, taskDto));
        }

        if (TypeScriptTask.MODE.equalsIgnoreCase(taskDto.getMode())) {
            return Optional.of(new TypeScriptTask(taskResultService, runtimeService, fileService, taskDto));
        }

        log.error("Unsupported mode - {}", taskDto.getMode());
        return Optional.empty();
    }
}
