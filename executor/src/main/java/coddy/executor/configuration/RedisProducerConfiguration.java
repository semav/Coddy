package coddy.executor.configuration;

import coddy.executor.dto.TaskResultDto;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;

@Configuration
public class RedisProducerConfiguration {
    @Bean
    RedisTemplate<String, TaskResultDto> redisTaskResultTemplate(RedisConnectionFactory connectionFactory, Jackson2JsonRedisSerializer<TaskResultDto> serializer) {
        RedisTemplate<String, TaskResultDto> redisTemplate = new RedisTemplate<>();
        redisTemplate.setConnectionFactory(connectionFactory);
        redisTemplate.setDefaultSerializer(serializer);
        redisTemplate.afterPropertiesSet();
        return redisTemplate;
    }

    @Bean
    public Jackson2JsonRedisSerializer<TaskResultDto> jackson2JsonRedisSerializer() {
        return new Jackson2JsonRedisSerializer<>(TaskResultDto.class);
    }
}
