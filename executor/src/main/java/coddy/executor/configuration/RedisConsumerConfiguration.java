package coddy.executor.configuration;

import coddy.executor.dto.TaskDto;
import coddy.executor.messaging.TaskListener;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.listener.PatternTopic;
import org.springframework.data.redis.listener.RedisMessageListenerContainer;
import org.springframework.data.redis.listener.adapter.MessageListenerAdapter;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;

@Configuration
public class RedisConsumerConfiguration {
    @Value("${executor.task.topic}")
    private String taskTopic;

    @Bean
    RedisMessageListenerContainer listenerContainer(RedisConnectionFactory connectionFactory, MessageListenerAdapter listenerAdapter) {
        RedisMessageListenerContainer container = new RedisMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.addMessageListener(listenerAdapter, new PatternTopic(taskTopic));

        return container;
    }

    @Bean
    MessageListenerAdapter listenerAdapter(TaskListener taskListener) {
        MessageListenerAdapter messageListenerAdapter = new MessageListenerAdapter(taskListener);
        messageListenerAdapter.setSerializer(new Jackson2JsonRedisSerializer<>(TaskDto.class));
        return messageListenerAdapter;
    }

}
