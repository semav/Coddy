package coddy.executor.messaging;

import coddy.executor.dto.TaskDto;
import coddy.executor.service.TaskExecutorService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
@RequiredArgsConstructor
public class TaskListener {
    private final TaskExecutorService taskExecutorService;

    public void handleMessage(TaskDto taskDto) {
        log.info("Handle task {}", taskDto);
        taskExecutorService.submit(taskDto);
    }
}
