package coddy.executor.messaging;

import coddy.executor.dto.TaskResultDto;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class TaskResultProducer {
    @Value("${executor.taskResult.topic}")
    private String taskResultTopic;

    private final RedisTemplate<String, TaskResultDto> redisTaskResultTemplate;

    public void send(TaskResultDto taskResultDto) {
        redisTaskResultTemplate.convertAndSend(taskResultTopic, taskResultDto);
    }
}
