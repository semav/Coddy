package coddy.executor;

import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class ExecutorApplication {

    public static void main(String[] args) throws InterruptedException {
        new SpringApplicationBuilder(ExecutorApplication.class)
                .web(WebApplicationType.NONE)
                .run(args);

        Thread.currentThread().join();
    }
}
